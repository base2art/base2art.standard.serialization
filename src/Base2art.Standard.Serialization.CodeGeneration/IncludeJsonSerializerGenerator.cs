﻿namespace Base2art.Serialization.CodeGeneration
{
    using System;
    using Microsoft.CodeAnalysis;
    using Microsoft.CodeAnalysis.CSharp;
    using Microsoft.CodeAnalysis.CSharp.Syntax;
    using System.Collections.Generic;
    using System.Collections.Immutable;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text.RegularExpressions;
    using System.Threading;
    using System.Threading.Tasks;
    using global::CodeGeneration.Roslyn;
    using Validation;
    using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

    public class IncludeJsonSerializerGenerator : ICodeGenerator
    {
        private readonly string targetNamespace;
        private readonly bool makePublic;

        private readonly ImmutableDictionary<string, TypedConstant> data;

        public static readonly DiagnosticDescriptor Descriptor = new DiagnosticDescriptor(
                                                                                          "B2ASER1",
                                                                                          "MayNeedToClean",
                                                                                          "You may need to run a dotnet/msbuild clean or build again "
                                                                                          + "to get a successful build.",
                                                                                          "Category1",
                                                                                          defaultSeverity: DiagnosticSeverity.Warning,
                                                                                          isEnabledByDefault: true,
                                                                                          description: "Rebuild may be needed",
                                                                                          helpLinkUri: "https://not-relevant.com",
                                                                                          customTags: new string [0]);

        public IncludeJsonSerializerGenerator(AttributeData attributeData)
        {
            Requires.NotNull(attributeData, nameof(attributeData));

            this.targetNamespace = (string) attributeData.ConstructorArguments[0].Value;
            this.data = attributeData.NamedArguments.ToImmutableDictionary(kv => kv.Key, kv => kv.Value);

            var name = nameof(IncludeJsonSerializerAttribute.MakePublic);
            if (this.data.ContainsKey(name))
            {
                this.makePublic = (bool) this.data[name].Value;
            }
        }

        public async Task<SyntaxList<MemberDeclarationSyntax>> GenerateAsync(
            TransformationContext context,
            IProgress<Diagnostic> progress,
            CancellationToken cancellationToken)
        {
            var names = new HashSet<string>(context.ProcessingNode.SyntaxTree.Options.PreprocessorSymbolNames, StringComparer.OrdinalIgnoreCase);

            bool writeFiles = names.Contains("DEBUG") || !names.Contains("RELEASE");

            if (names.Contains("RELEASE"))
            {
                writeFiles = false;
            }

            var asm = typeof(IncludeJsonSerializerGenerator).GetTypeInfo().Assembly;

            var basePath = Path.Combine(context.ProjectDirectory, "Serialization.gen");

            var version = GetVersion(asm);
            var versionFile = Path.Combine(basePath, $"{version}.meta");

            if (Directory.Exists(basePath))
            {
                if (File.Exists(versionFile))
                {
                    return SyntaxFactory.List<MemberDeclarationSyntax>();
                }

                Directory.Delete(basePath, true);
            }

            if (writeFiles)
            {
                Directory.CreateDirectory(basePath);

                File.WriteAllText(Path.Combine(basePath, ".gitignore"), "*.cs\n*/*.cs\n*.meta\n.gitignore");
            }

            var filesAsync = this.GetAllFiles(asm).Select(async x =>
            {
                var leadingTrivia = "";
                var trailingTrivia = "";

                if (x.StartsWith("#if"))
                {
                    leadingTrivia = new string(x.TakeWhile(y => y != '\n').ToArray()).Trim() + Environment.NewLine;
                    trailingTrivia = $"{Environment.NewLine}#endif";
                }

                var tree = CSharpSyntaxTree.ParseText(x, CSharpParseOptions.Default.WithPreprocessorSymbols("NETSTANDARD2_0"));

                var compilationUnitSyntax = (CompilationUnitSyntax) await tree.GetRootAsync(cancellationToken);

                return Tuple.Create(leadingTrivia, compilationUnitSyntax, trailingTrivia);
            });

            var files = await Task.WhenAll(filesAsync);

            var (nsLookup, classLookup) = this.ChangeNamespaceAsync(List<MemberDeclarationSyntax>(), files, this.targetNamespace);

            List<string> paths = new List<string>();
            foreach (var item in classLookup)
            {
                var items = new List<string> {basePath};

                var parts = item.Item1.Split('.')
                                .Select(x => x.Trim())
                                .Where(x => !string.IsNullOrWhiteSpace(x))
                                .ToArray();

                if (parts.Length > 0)
                {
                    items.AddRange(parts);
                }

                var dirPath = Path.Combine(items.ToArray());

                Directory.CreateDirectory(dirPath);

                try
                {
                    var path = Path.Combine(dirPath, $"{item.Item2}.cs");
                    if (writeFiles)
                    {
                        paths.Add(path);
                        File.AppendAllText(
                                           path,
                                           $"{item.Item3.ToFullString()}");
                    }
                }
                catch (Exception e)
                {
                    throw new InvalidOperationException($"`{dirPath}\\{item.Item2}.cs`", e);
                }
            }

            if (!writeFiles)
            {
                return nsLookup;
            }

            var diagnostics = Diagnostic.Create(
                                                Descriptor,
                                                Location.Create(context.ProcessingNode.SyntaxTree, context.ProcessingNode.Span));

            progress.Report(diagnostics);
            File.WriteAllText(versionFile, version);
            return new SyntaxList<MemberDeclarationSyntax>();
        }

        private IEnumerable<string> GetAllFiles(Assembly asm)
        {
            var resources = asm.GetManifestResourceNames();
            foreach (var resource in resources)
            {
                using (var stream = asm.GetManifestResourceStream(resource))
                {
                    using (var ssr = new System.IO.StreamReader(stream))
                    {
                        yield return ssr.ReadToEnd().Trim();
                    }
                }
            }
        }

        private static string GetVersion(Assembly asm)
        {
            var version = "0.0.0.1";
            var loc = new FileInfo(asm.Location).Directory;

            var matcher = new Regex(@"^\d{1,5}\.\d{1,5}(\.\d{1,5}(\.\d{1,5})?)?$");
            while (loc != null && !matcher.IsMatch(loc.Name))
            {
                loc = loc.Parent;
            }

            if (loc != null)
            {
                version = loc.Name;
            }

            return version;
        }

        private (SyntaxList<MemberDeclarationSyntax>, List<(string, string, NamespaceDeclarationSyntax)>) ChangeNamespaceAsync(
            SyntaxList<MemberDeclarationSyntax> items,
            Tuple<string, CompilationUnitSyntax, string>[] codeItems,
            string @namespace)
        {
            var nsLookup = List<MemberDeclarationSyntax>();

            List<(string, string, NamespaceDeclarationSyntax)> classLookup = new List<(string, string, NamespaceDeclarationSyntax)>();

            foreach (var codeItem in codeItems)
            {
                foreach (var fileMember in codeItem.Item2.Members)
                {
                    if (fileMember is NamespaceDeclarationSyntax namespaceDeclarationSyntax)
                    {
                        var name = namespaceDeclarationSyntax.Name.ToFullString();
                        if (!string.IsNullOrWhiteSpace(name))
                        {
                            nsLookup = nsLookup.Add(this.ProcessNamespace(
                                                                          name,
                                                                          codeItem.Item1,
                                                                          namespaceDeclarationSyntax,
                                                                          codeItem.Item3,
                                                                          classLookup,
                                                                          @namespace));
                        }
                    }
                }
            }

            return (nsLookup, classLookup);
        }

        private MemberDeclarationSyntax ProcessNamespace(
            string name,
            string leading,
            NamespaceDeclarationSyntax fileMember,
            string trailing,
            List<(string, string, NamespaceDeclarationSyntax)> classLookup,
            string @namespace)
        {
            var key = name;

            var nameName = key.Replace("Base2art.Serialization", @namespace);

            var newNamespace = SyntaxFactory.NamespaceDeclaration(SyntaxFactory.ParseName(nameName))
                                            .WithTriviaFrom(fileMember)
                                            .WithLeadingTrivia(Comment(leading))
                                            .WithTrailingTrivia(Comment(trailing))
                                            .NormalizeWhitespace();

            foreach (var nsMember in fileMember.Members)
            {
                if (nsMember is TypeDeclarationSyntax typeDeclarationSyntax)
                {
                    newNamespace = this.ProcessClass(
                                                     key,
                                                     leading,
                                                     fileMember,
                                                     trailing,
                                                     typeDeclarationSyntax,
                                                     newNamespace,
                                                     classLookup,
                                                     nameName);
                }
                else
                {
                    newNamespace = newNamespace.AddMembers(nsMember);
                }
            }

            return newNamespace;
        }

        private NamespaceDeclarationSyntax ProcessClass(
            string originalNamespace,
            string leading,
            NamespaceDeclarationSyntax nsMember,
            string trailing,
            TypeDeclarationSyntax classDeclaration,
            NamespaceDeclarationSyntax newNamespace,
            List<(string, string, NamespaceDeclarationSyntax)> classLookup,
            string newName)
        {
            string typeNameForFile(TypeDeclarationSyntax z)
            {
                return new string(z.Identifier.ToFullString().TakeWhile(x => x != '<').ToArray()).Trim();
            }

            var newNamespaceForClass = SyntaxFactory.NamespaceDeclaration(SyntaxFactory.ParseName(newName))
                                                    .WithTriviaFrom(nsMember)
                                                    .WithLeadingTrivia(
                                                                       Comment(leading),
                                                                       Comment(@"// ReSharper disable once CheckNamespace"),
                                                                       Comment(@"// THIS CODE IS GENERATED DO NOT EDIT!!!"))
                                                    .WithTrailingTrivia(Comment(trailing))
                                                    .NormalizeWhitespace();

            if (makePublic)
            {
                newNamespace = newNamespace.AddUsings(nsMember.Usings.ToArray())
                                           .AddMembers(classDeclaration)
                                           .NormalizeWhitespace();

                newNamespaceForClass = newNamespaceForClass.AddUsings(nsMember.Usings.ToArray())
                                                           .AddMembers(classDeclaration)
                                                           .NormalizeWhitespace();
            }
            else
            {
                if (classDeclaration.Modifiers.Any(x => x.IsKind(SyntaxKind.PublicKeyword)))
                {
                    var mod = classDeclaration.Modifiers.First(x => x.IsKind(SyntaxKind.PublicKeyword));

                    var newModifiers = classDeclaration.Modifiers.Replace(
                                                                          mod,
                                                                          SyntaxFactory.Token(SyntaxKind.InternalKeyword)
                                                                                       .WithTriviaFrom(mod));

                    var classDeclaration1 = classDeclaration.WithModifiers(newModifiers)
                                                            .WithTriviaFrom(classDeclaration);

                    newNamespace = newNamespace.AddUsings(nsMember.Usings.ToArray())
                                               .AddMembers(classDeclaration1)
                                               .NormalizeWhitespace();

                    newNamespaceForClass = newNamespaceForClass.AddUsings(nsMember.Usings.ToArray())
                                                               .AddMembers(classDeclaration1)
                                                               .NormalizeWhitespace();
                }
                else
                {
                    newNamespace = newNamespace.AddUsings(nsMember.Usings.ToArray())
                                               .AddMembers(classDeclaration)
                                               .NormalizeWhitespace();

                    newNamespaceForClass = newNamespaceForClass.AddUsings(nsMember.Usings.ToArray())
                                                               .AddMembers(classDeclaration)
                                                               .NormalizeWhitespace();
                }

                var ns = originalNamespace.Replace("Base2art.Serialization", "");
                classLookup.Add((ns, typeNameForFile(classDeclaration), newNamespaceForClass));
            }

            return newNamespace;
        }
    }
}

/*
 
 

//                    var classDeclarations = member.Members
//                                                  .OfType<TypeDeclarationSyntax>()
//                                                  .ToArray();

//                    }
//                }
//            nsLookup = nsLookup.Add(newNamespace);
        private NamespaceDeclarationSyntax FakeMember(string suffix)
        {
            var variableDeclarationSyntax = VariableDeclaration(PredefinedType(Token(SyntaxKind.StringKeyword)))
                .AddVariables(
                              VariableDeclarator(Identifier("Path" + suffix))
                                  .WithInitializer(
                                                   EqualsValueClause(
                                                                     LiteralExpression(
                                                                                       SyntaxKind.StringLiteralExpression,
                                                                                       Literal("FAKE")))));
            return NamespaceDeclaration(SyntaxFactory.ParseName("TestNS"))
                .WithMembers(List<MemberDeclarationSyntax>().Add(ClassDeclaration("DirectoryPathTest")
                                                                     .AddMembers(
                                                                                 FieldDeclaration(variableDeclarationSyntax)
                                                                                     .WithModifiers(TokenList(Token(SyntaxKind.PublicKeyword),
                                                                                                              Token(SyntaxKind.ConstKeyword))))));
        }
*/