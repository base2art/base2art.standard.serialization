namespace Base2art.Serialization.CodeGeneration
{
    using System;
    using System.Diagnostics;
    using global::CodeGeneration.Roslyn;
    using Validation;

    [AttributeUsage(AttributeTargets.Assembly, Inherited = false, AllowMultiple = false)]
    [CodeGenerationAttribute(typeof(IncludeJsonSerializerGenerator))]
    [Conditional("CodeGeneration")]
    public class IncludeJsonSerializerAttribute : Attribute
    {
        public bool MakePublic { get; set; }

        public string TargetNamespace { get; set; }

        public IncludeJsonSerializerAttribute(string targetNamespace)
        {
            Requires.NotNullOrEmpty(targetNamespace, nameof(targetNamespace));
            this.TargetNamespace = targetNamespace;
        }
    }
}