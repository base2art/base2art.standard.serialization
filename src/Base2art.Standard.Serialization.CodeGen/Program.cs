﻿[assembly: Base2art.Serialization.CodeGeneration.IncludeJsonSerializer("Base2art.Serialization.CodeGen", MakePublic = false)]

namespace Base2art.Serialization.CodeGen
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Threading.Tasks;
    using Microsoft.CodeAnalysis;
    using Serialization.CodeGen;

    class Program
    {
        static Task<int> Main(string[] args)
        {
//            Console.WriteLine(TestNS.DirectoryPathTest.Path);
            Console.WriteLine("");
            var serializer = new CamelCasingSimpleJsonSerializer();
            return Task.FromResult(0);
        }
    }
}