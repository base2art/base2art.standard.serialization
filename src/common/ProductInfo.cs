// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyProduct("Base2art.Standard.Serialization")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.1")]
[assembly: ComVisible(false)]