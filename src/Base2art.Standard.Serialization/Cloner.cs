﻿namespace Base2art.Serialization
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Reflection;

    /// <summary>
    ///     A Class that knows how to deep clone objects.
    /// </summary>
    public static class Cloner
    {
        /// <summary>
        ///     A method that deep clones objects.
        /// </summary>
        /// <param name="item">The item to clone.</param>
        /// <typeparam name="T">The type of item to clone.</typeparam>
        /// <returns>The new cloned object.</returns>
        public static T CloneObject<T>(this T item) => item.CloneObject(new SimpleJsonSerializer());

        /// <summary>
        ///     A method that deep clones objects.
        /// </summary>
        /// <param name="item">The item to clone.</param>
        /// <param name="serializer">A custom serializer to serialize and deserialize.</param>
        /// <typeparam name="T">The type of item to clone.</typeparam>
        /// <returns>The new cloned object.</returns>
        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "SjY")]
        public static T CloneObject<T>(this T item, IJsonSerializer serializer)
        {
            if (item == null)
            {
                return default(T);
            }

            if (item.GetType().GetTypeInfo().IsValueType)
            {
                return item;
            }

            if (serializer == null)
            {
                return item.CloneObject(new SimpleJsonSerializer());
            }

            try
            {
                var s = serializer.Serialize(item);
                var rez = serializer.Deserialize<T>(s);
                return rez;
            }
            catch (Exception ex)
            {
                throw new SerializationException("No Serializer found", ex);
            }
        }
    }
}