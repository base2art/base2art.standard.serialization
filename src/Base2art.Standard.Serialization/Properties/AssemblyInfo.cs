using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyTitle("Base2art.Standard.Serialization")]
[assembly: AssemblyDescription("Set of classes, modules and libraries for easier implementation of serialization features.")]

[assembly: AssemblyConfiguration("")]
[assembly: InternalsVisibleTo("Base2art.Standard.Serialization.BuiltIn.Features")]

[assembly:
    SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace",
        Target = "Base2art.Collections", Justification = "SjY")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace",
        Target = "Base2art.IO", Justification = "SjY")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace",
        Target = "Base2art.Net", Justification = "SjY")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace",
        Target = "Base2art.Security", Justification = "SjY")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace",
        Target = "Base2art.Serialization", Justification = "SjY")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace",
        Target = "Base2art.Text", Justification = "SjY")]