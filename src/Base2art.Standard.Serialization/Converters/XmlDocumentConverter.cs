﻿#if !NETSTANDARD1_0
namespace Base2art.Serialization.Converters
{
    using System.IO;
    using System.Xml;

    /// <summary>
    ///     A Regex serializer and deserializer.
    /// </summary>
    public class XmlDocumentConverter : ConverterBase<XmlDocument>
    {
        /// <summary>
        ///     Convert an object to it's serialized form.
        /// </summary>
        /// <param name="value">The Object to serialize.</param>
        /// <returns>The serialized representation.</returns>
        protected override object SerializeObject(XmlDocument value)
        {
            using (var sw = new StringWriter())
            {
                value.Save(sw);
                sw.Flush();
                var result = sw.GetStringBuilder().ToString();
                return result;
            }
        }

        /// <summary>
        ///     Convert an object to it's serializable form.
        /// </summary>
        /// <param name="value">The Object to deserialize.</param>
        /// <returns>The deserialized representation.</returns>
        protected override XmlDocument DeserializeObject(object value)
        {
            if (!(value is string jvalue))
            {
                return null;
            }

            var doc = new XmlDocument();
            doc.LoadXml(jvalue);
            return doc;
        }
    }
}

#endif