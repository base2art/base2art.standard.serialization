﻿namespace Base2art.Serialization.Converters
{
    using System;

    /// <summary>
    ///     A TimeSpan serializer and deserializer.
    /// </summary>
    public class TimeSpanConverter : ConverterBase<TimeSpan>
    {
        /// <summary>
        ///     Convert an object to it's serialized form.
        /// </summary>
        /// <param name="value">The Object to serialize.</param>
        /// <returns>The serialized representation.</returns>
        protected override object SerializeObject(TimeSpan value) => value.ToString();

        /// <summary>
        ///     Convert an object to it's serializable form.
        /// </summary>
        /// <param name="value">The Object to deserialize.</param>
        /// <returns>The deserialized representation.</returns>
        protected override TimeSpan DeserializeObject(object value)
        {
            TimeSpan.TryParse((string) value, out var output);
            return output;
        }
    }
}