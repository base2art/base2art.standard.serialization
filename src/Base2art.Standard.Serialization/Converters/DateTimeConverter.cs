﻿namespace Base2art.Serialization.Converters
{
    using System;

    /// <summary>
    ///     Converts a date time.
    /// </summary>
    public class DateTimeConverter : SerializingConverterBase<DateTime>
    {
        private const string DefaultDateTimeFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.FFFFFFFK";

        /// <summary>
        ///     Convert an object to it's serialized form.
        /// </summary>
        /// <param name="value">The Object to serialize.</param>
        /// <returns>The serialized representation.</returns>
        protected override string SerializeObject(DateTime value) => value.ToString(DefaultDateTimeFormat);
    }
}