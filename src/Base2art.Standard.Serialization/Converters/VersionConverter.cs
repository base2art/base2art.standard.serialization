﻿namespace Base2art.Serialization.Converters
{
    using System;

    /// <summary>
    ///     A Version serializer and deserializer.
    /// </summary>
    public class VersionConverter : ConverterBase<Version>
    {
        /// <summary>
        ///     Convert an object to it's serialized form.
        /// </summary>
        /// <param name="value">The Object to serialize.</param>
        /// <returns>The serialized representation.</returns>
        protected override object SerializeObject(Version value) => value.ToString();

        /// <summary>
        ///     Convert an object to it's serializable form.
        /// </summary>
        /// <param name="value">The Object to deserialize.</param>
        /// <returns>The deserialized representation.</returns>
        protected override Version DeserializeObject(object value)
        {
            Version.TryParse((string) value, out var output);
            return output;
        }
    }
}