﻿namespace Base2art.Serialization.Converters
{
    using System;

    /// <summary>
    ///     A Version serializer and deserializer.
    /// </summary>
    public class ByteArrayConverter : ConverterBase<byte[]>
    {
        /// <summary>
        ///     Convert an object to it's serialized form.
        /// </summary>
        /// <param name="value">The Object to serialize.</param>
        /// <returns>The serialized representation.</returns>
        protected override object SerializeObject(byte[] value) => value == null ? null : Convert.ToBase64String(value);

        /// <summary>
        ///     Convert an object to it's serializable form.
        /// </summary>
        /// <param name="value">The Object to deserialize.</param>
        /// <returns>The deserialized representation.</returns>
        protected override byte[] DeserializeObject(object value) => value == null ? null : Convert.FromBase64String(value.ToString());
    }
}