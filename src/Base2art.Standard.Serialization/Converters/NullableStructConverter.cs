﻿namespace Base2art.Serialization.Converters
{
    using System;
    using Internals;

    /// <summary>
    ///     A Nullable Struct serializer and deserializer.
    ///     This class is meant to wrap serializers for structs.
    /// </summary>
    public class NullableStructConverter : IConverter
    {
        private readonly IConverter baseConverter;

        /// <summary>
        ///     Creates a new instance of the <see cref="NullableStructConverter" /> class.
        /// </summary>
        /// <param name="baseConverter">The wrapped struct converter.</param>
        public NullableStructConverter(IConverter baseConverter) => this.baseConverter = baseConverter;

        /// <summary>
        ///     A method that determines whether this converter can deserialize the given type.
        /// </summary>
        /// <param name="type">The type to evalutate for deserialization.</param>
        /// <returns>A value indicating that the object can be deserialized.</returns>
        public bool CanDeserialize(Type type) => type.IsNullable() && this.baseConverter.CanDeserialize(type.GenericTypeArguments[0]);

        /// <summary>
        ///     Convert an object to it's serializable form.
        /// </summary>
        /// <param name="type">The type of object to deserialize.</param>
        /// <param name="value">The Object to deserialize.</param>
        /// <returns>The deserialized representation.</returns>
        public object Deserialize(Type type, object value) =>
            value == null ? null : this.baseConverter.Deserialize(type.GenericTypeArguments[0], value);

        /// <summary>
        ///     A method that determines whether this converter can serialize the given object.
        /// </summary>
        /// <param name="objectType">The type of object to serialize.</param>
        /// <param name="value">The object to evalutate for serialization.</param>
        /// <returns>A value indicating that the object can be deserialized.</returns>
        public bool CanSerialize(Type objectType, object value) =>
            objectType.IsNullable() && this.baseConverter.CanDeserialize(objectType.GenericTypeArguments[0]);

        /// <summary>
        ///     Convert an object to it's serialized form.
        /// </summary>
        /// <param name="objectType">The type of object to serialize.</param>
        /// <param name="value">The Object to serialize.</param>
        /// <returns>The serialized representation.</returns>
        public object Serialize(Type objectType, object value) => this.baseConverter.Serialize(objectType, value);
    }
}