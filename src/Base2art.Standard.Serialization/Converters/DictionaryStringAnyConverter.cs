﻿namespace Base2art.Serialization.Converters
{
    using System;

    /// <summary>
    ///     A Dictionary deserializer.
    /// </summary>
    public class DictionaryStringAnyConverter : DictionaryAnyAnyConverterBase
    {
        public DictionaryStringAnyConverter(IJsonSerializer serializer) : base(serializer)
        {
        }

        protected override bool IsMatch(Type first) => first == typeof(string);

        protected override string SerializeKey(Type objectType, object value) => (string) value;

        protected override object DeserializeKey(Type objectType, string key) => key;
    }
}