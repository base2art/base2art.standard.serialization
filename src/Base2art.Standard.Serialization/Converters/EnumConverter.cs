﻿namespace Base2art.Serialization.Converters
{
    using System;
    using System.Reflection;

    /// <summary>
    ///     An enum serializer and deserializer.
    /// </summary>
    public class EnumConverter : IConverter
    {
        /// <summary>
        ///     A method that determines whether this converter can deserialize the given type.
        /// </summary>
        /// <param name="type">The type to evalutate for deserialization.</param>
        /// <returns>A value indicating that the object can be deserialized.</returns>
        public bool CanDeserialize(Type type) => type.GetTypeInfo().IsEnum;

        /// <summary>
        ///     Convert an object to it's serializable form.
        /// </summary>
        /// <param name="type">The type of object to deserialize.</param>
        /// <param name="value">The Object to deserialize.</param>
        /// <returns>The deserialized representation.</returns>
        public object Deserialize(Type type, object value) => this.DeserializeObject(type, value);

        /// <summary>
        ///     A method that determines whether this converter can serialize the given object.
        /// </summary>
        /// <param name="objectType">The type of object to serialize.</param>
        /// <param name="value">The object to evalutate for serialization.</param>
        /// <returns>A value indicating that the object can be deserialized.</returns>
        public bool CanSerialize(Type objectType, object value) => objectType.GetTypeInfo().IsEnum;

        /// <summary>
        ///     Convert an object to it's serialized form.
        /// </summary>
        /// <param name="objectType">The type of object to serialize.</param>
        /// <param name="value">The Object to serialize.</param>
        /// <returns>The serialized representation.</returns>
        public object Serialize(Type objectType, object value) => ((Enum) value)?.ToString("G");

        private object DeserializeObject(Type type, object value)
        {
            try
            {
                return Enum.Parse(type, (string) value);
            }
            catch (Exception)
            {
                return this.DefaultValue(type);
            }
        }

        private object DefaultValue(Type type)
        {
            var items = Enum.GetValues(type);

            return items.GetValue(0);
        }
    }
}