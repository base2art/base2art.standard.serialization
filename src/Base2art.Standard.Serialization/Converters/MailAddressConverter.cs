﻿#if NETSTANDARD2_0
namespace Base2art.Serialization.Converters
{
    using System;
    using System.Net.Mail;

    /// <summary>
    ///     A MailAddress serializer and deserializer.
    /// </summary>
    public class MailAddressConverter : ConverterBase<MailAddress>
    {
        /// <summary>
        ///     Convert an object to it's serialized form.
        /// </summary>
        /// <param name="value">The Object to serialize.</param>
        /// <returns>The serialized representation.</returns>
        protected override object SerializeObject(MailAddress value) => value.ToString();

        /// <summary>
        ///     Convert an object to it's serializable form.
        /// </summary>
        /// <param name="value">The Object to deserialize.</param>
        /// <returns>The deserialized representation.</returns>
        protected override MailAddress DeserializeObject(object value)
        {
            try
            {
                return new MailAddress((string) value);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
#endif