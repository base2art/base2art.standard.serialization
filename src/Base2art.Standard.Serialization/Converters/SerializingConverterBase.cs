﻿namespace Base2art.Serialization.Converters
{
    using System;

    /// <summary>
    ///     A Base class to override when only serialization is necessary.
    /// </summary>
    /// <typeparam name="T">the type of object to convert</typeparam>
    public abstract class SerializingConverterBase<T> : IConverter
    {
        /// <summary>
        ///     A method that determines whether this converter can deserialize the given type.
        /// </summary>
        /// <param name="type">The type to evalutate for deserialization.</param>
        /// <returns>A value indicating that the object can be deserialized.</returns>
        public bool CanDeserialize(Type type) => false;

        /// <summary>
        ///     Convert an object to it's serializable form.
        /// </summary>
        /// <param name="type">The type of object to deserialize.</param>
        /// <param name="value">The Object to deserialize.</param>
        /// <returns>The deserialized representation.</returns>
        public object Deserialize(Type type, object value) => throw new NotImplementedException();

        /// <summary>
        ///     A method that determines whether this converter can serialize the given object.
        /// </summary>
        /// <param name="objectType">The type of object to serialize.</param>
        /// <param name="value">The object to evalutate for serialization.</param>
        /// <returns>A value indicating that the object can be deserialized.</returns>
        public bool CanSerialize(Type objectType, object value) => objectType == typeof(T);

        /// <summary>
        ///     Convert an object to it's serialized form.
        /// </summary>
        /// <param name="objectType">The type of object to serialize.</param>
        /// <param name="value">The Object to serialize.</param>
        /// <returns>The serialized representation.</returns>
        public object Serialize(Type objectType, object value) => this.SerializeObject((T) value);

        /// <summary>
        ///     Convert an object to it's serialized form.
        /// </summary>
        /// <param name="value">The Object to serialize.</param>
        /// <returns>The serialized representation.</returns>
        protected abstract string SerializeObject(T value);
    }
}