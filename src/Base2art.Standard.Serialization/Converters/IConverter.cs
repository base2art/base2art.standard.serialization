﻿namespace Base2art.Serialization.Converters
{
    using System;

    /// <summary>
    ///     A contract that specifies how to convert objects for serialization.
    /// </summary>
    public interface IConverter
    {
        /// <summary>
        ///     A method that determines whether this converter can deserialize the given type.
        /// </summary>
        /// <param name="type">The type to evalutate for deserialization.</param>
        /// <returns>A value indicating that the object can be deserialized.</returns>
        bool CanDeserialize(Type type);

        /// <summary>
        ///     Convert an object to it's serializable form.
        /// </summary>
        /// <param name="type">The type of object to deserialize.</param>
        /// <param name="value">The Object to deserialize.</param>
        /// <returns>The deserialized representation.</returns>
        object Deserialize(Type type, object value);

        /// <summary>
        ///     A method that determines whether this converter can serialize the given object.
        /// </summary>
        /// <param name="objectType">The type of object to serialize.</param>
        /// <param name="value">The object to evalutate for serialization.</param>
        /// <returns>A value indicating that the object can be deserialized.</returns>
        bool CanSerialize(Type objectType, object value);

        /// <summary>
        ///     Convert an object to it's serialized form.
        /// </summary>
        /// <param name="objectType">The type of object to serialize.</param>
        /// <param name="value">The Object to serialize.</param>
        /// <returns>The serialized representation.</returns>
        object Serialize(Type objectType, object value);
    }
}