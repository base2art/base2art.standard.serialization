﻿namespace Base2art.Serialization.Converters
{
    using System;
    using System.Text.RegularExpressions;
    using Internals;

    /// <summary>
    ///     A Regex serializer and deserializer.
    /// </summary>
    public class RegexConverter : ConverterBase<Regex>
    {
        private const string PatternName = "Pattern";
        private const string OptionsName = "Options";

        private readonly IJsonSerializer serializer;

        /// <summary>
        ///     Creates a new instance of the <see cref="RegexConverter" /> class.
        /// </summary>
        /// <param name="serializer">The backing serializer.</param>
        public RegexConverter(IJsonSerializer serializer) => this.serializer = serializer;

        /// <summary>
        ///     Convert an object to it's serialized form.
        /// </summary>
        /// <param name="value">The Object to serialize.</param>
        /// <returns>The serialized representation.</returns>
        protected override object SerializeObject(Regex value) => new {Pattern = value.ToString(), Options = value.Options.ToString("G")};

        /// <summary>
        ///     Convert an object to it's serializable form.
        /// </summary>
        /// <param name="value">The Object to deserialize.</param>
        /// <returns>The deserialized representation.</returns>
        protected override Regex DeserializeObject(object value)
        {
            var patternName = this.serializer.MapName(typeof(Regex), PatternName);
            var optionsName = this.serializer.MapName(typeof(Regex), OptionsName);
            if (!(value is JsonObject jvalue) || !jvalue.ContainsKey(patternName))
            {
                return null;
            }

            var pattern = (jvalue[patternName] ?? string.Empty).ToString();
            var options = RegexOptions.None;

            if (jvalue.ContainsKey(optionsName))
            {
                options = (RegexOptions) Enum.Parse(typeof(RegexOptions), (jvalue[optionsName] ?? string.Empty).ToString(), true);
            }

            return new Regex(pattern, options);
        }
    }
}