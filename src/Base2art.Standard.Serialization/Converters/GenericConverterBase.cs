﻿namespace Base2art.Serialization.Converters
{
    using System;
    using System.Reflection;

    /// <summary>
    ///     A base class used for serializing and deserializing Generics,
    ///     such as KeyValuePair&lt;TKey, TValue&gt;
    /// </summary>
    public abstract class GenericConverterBase : IConverter
    {
        private readonly Type rawType;

        /// <summary>
        ///     Creates a new instance of the <see cref="GenericConverterBase" /> class.
        /// </summary>
        /// <param name="serializer">The overal serializer.</param>
        /// <param name="rawType">The generic type that is supported.</param>
        protected GenericConverterBase(IJsonSerializer serializer, Type rawType)
        {
            this.Serializer = serializer;
            this.rawType = rawType;
        }

        /// <summary>
        ///     Gets the underlying serializer.
        /// </summary>
        protected IJsonSerializer Serializer { get; }

        /// <summary>
        ///     A method that determines whether this converter can deserialize the given type.
        /// </summary>
        /// <param name="type">The type to evalutate for deserialization.</param>
        /// <returns>A value indicating that the object can be deserialized.</returns>
        public virtual bool CanDeserialize(Type type) => type.GetTypeInfo().IsGenericType && this.rawType == type.GetGenericTypeDefinition();

        /// <summary>
        ///     Convert an object to it's serializable form.
        /// </summary>
        /// <param name="type">The type of object to deserialize.</param>
        /// <param name="value">The Object to deserialize.</param>
        /// <returns>The deserialized representation.</returns>
        public object Deserialize(Type type, object value) => value == null ? null : this.DeserializeObject(type, value);

        /// <summary>
        ///     A method that determines whether this converter can serialize the given object.
        /// </summary>
        /// <param name="objectType">The type of object to serialize.</param>
        /// <param name="value">The object to evalutate for serialization.</param>
        /// <returns>A value indicating that the object can be deserialized.</returns>
        public virtual bool CanSerialize(Type objectType, object value) =>
            objectType.GetTypeInfo().IsGenericType && this.rawType == objectType.GetGenericTypeDefinition();

        /// <summary>
        ///     Convert an object to it's serialized form.
        /// </summary>
        /// <param name="objectType">The type of object to serialize.</param>
        /// <param name="value">The Object to serialize.</param>
        /// <returns>The serialized representation.</returns>
        public object Serialize(Type objectType, object value) => this.SerializeObject(value);

        /// <summary>
        ///     Convert an object to it's serialized form.
        /// </summary>
        /// <param name="value">The Object to serialize.</param>
        /// <returns>The serialized representation.</returns>
        protected abstract object SerializeObject(object value);

        /// <summary>
        ///     Convert an object to it's serializable form.
        /// </summary>
        /// <param name="type">The type of object to deserialize.</param>
        /// <param name="value">The Object to deserialize.</param>
        /// <returns>The deserialized representation.</returns>
        protected abstract object DeserializeObject(Type type, object value);
    }
}