﻿namespace Base2art.Serialization.Converters
{
    using System.IO;
    using System.Xml.Linq;

    /// <summary>
    ///     A Regex serializer and deserializer.
    /// </summary>
    public class XElementConverter : ConverterBase<XElement>
    {
        /// <summary>
        ///     Convert an object to it's serialized form.
        /// </summary>
        /// <param name="value">The Object to serialize.</param>
        /// <returns>The serialized representation.</returns>
        protected override object SerializeObject(XElement value)
        {
            using (var sw = new StringWriter())
            {
                value.Save(sw);
                sw.Flush();
                return sw.GetStringBuilder().ToString();
            }
        }

        /// <summary>
        ///     Convert an object to it's serializable form.
        /// </summary>
        /// <param name="value">The Object to deserialize.</param>
        /// <returns>The deserialized representation.</returns>
        protected override XElement DeserializeObject(object value)
        {
            if (!(value is string jvalue))
            {
                return null;
            }

            return XElement.Parse(jvalue);
        }
    }
}