﻿namespace Base2art.Serialization.Converters
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using Internals;

    /// <summary>
    ///     A KeyValuePair serializer and deserializer.
    /// </summary>
    public class KeyValuePairConverter : GenericConverterBase
    {
        private const string Key = "Key";
        private const string Value = "Value";

        /// <summary>
        ///     Creates a new instance of the <see cref="KeyValuePairConverter" /> class.
        /// </summary>
        /// <param name="serializer">The backing serializer.</param>
        public KeyValuePairConverter(IJsonSerializer serializer)
            : base(serializer, typeof(KeyValuePair<,>))
        {
        }

        /// <summary>
        ///     Convert an object to it's serialized form.
        /// </summary>
        /// <param name="value">The Object to serialize.</param>
        /// <returns>The serialized representation.</returns>
        protected override object SerializeObject(object value)
        {
            var inputType = value.GetType();
            var keyCaller = value.GetType().GetRuntimeProperty("Key").GetMethod;
            var valueCaller = value.GetType().GetRuntimeProperty("Value").GetMethod;

            // REVIEW FOR SPEED
            return new {Key = keyCaller.Invoke(value, null), Value = valueCaller.Invoke(value, null)};
        }

        /// <summary>
        ///     Convert an object to it's serializable form.
        /// </summary>
        /// <param name="type">The type of object to deserialize.</param>
        /// <param name="value">The Object to deserialize.</param>
        /// <returns>The deserialized representation.</returns>
        protected override object DeserializeObject(Type type, object value)
        {
            var keyName = this.Serializer.MapName(type, Key);
            var valueName = this.Serializer.MapName(type, Value);
            if (!(value is JsonObject jvalue) || !jvalue.ContainsKey(keyName) || !jvalue.ContainsKey(valueName))
            {
                return null;
            }

            var ikey = jvalue[keyName];
            var ivalue = jvalue[valueName];

            var typeInfo = type.GetTypeInfo();
            var keyType = typeInfo.GenericTypeArguments[0];
            var valueType = typeInfo.GenericTypeArguments[1];

            ikey = Convert.ChangeType(ikey, keyType);
            ivalue = Convert.ChangeType(ivalue, valueType);

            return Activator.CreateInstance(type, ikey, ivalue);
        }
    }
}