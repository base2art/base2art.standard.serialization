﻿namespace Base2art.Serialization
{
    using System;
    using Converters;
    using Internals;

    /// <summary>
    ///     A simple serializer that allows the user to configure how the output is serialised.
    /// </summary>
    public sealed class ConfigurableSimpleJsonSerializer : IJsonSerializer
    {
        private Func<Type, string, bool> augmentClrPropertyName;
        private readonly bool camelCaseOutput;
        private readonly bool indented;
        private readonly Func<IJsonSerializer, IConverter[]> converters;
        private readonly Func<Type, string, string> newClrPropertyName;

        /// <summary>
        ///     Creates a new instance of the <see cref="ConfigurableSimpleJsonSerializer" /> class.
        /// </summary>
        /// <param name="camelCaseOutput">A value indicating that the output should be camelCased.</param>
        /// <param name="indented">A value indicating that the output should be indented.</param>
        /// <param name="converters">A custom set of converters that can augment how data is serialized.</param>
        public ConfigurableSimpleJsonSerializer(
            bool camelCaseOutput,
            bool indented,
            Func<IJsonSerializer, IConverter[]> converters)
            : this(camelCaseOutput, indented, converters, null, null)
        {
        }

        /// <summary>
        ///     Creates a new instance of the <see cref="ConfigurableSimpleJsonSerializer" /> class.
        /// </summary>
        /// <param name="camelCaseOutput">A value indicating that the output should be camelCased.</param>
        /// <param name="indented">A value indicating that the output should be indented.</param>
        /// <param name="converters">A custom set of converters that can augment how data is serialized.</param>
        public ConfigurableSimpleJsonSerializer(
            bool camelCaseOutput,
            bool indented,
            Func<IJsonSerializer, IConverter[]> converters,
            Func<Type, string, bool> augmentClrPropertyName,
            Func<Type, string, string> newClrPropertyName)
        {
            this.augmentClrPropertyName = augmentClrPropertyName;
            this.camelCaseOutput = camelCaseOutput;
            this.indented = indented;
            this.converters = converters;
            this.newClrPropertyName = newClrPropertyName;
        }

        /// <summary>
        ///     Serialize an object to a string.
        /// </summary>
        /// <param name="item">The item to serialize.</param>
        /// <typeparam name="T">The type of object to serialize.</typeparam>
        /// <returns>The serialized value.</returns>
        public string Serialize<T>(T item) => SimpleJson.SerializeObject(item, this.CreateSerializerStrategy());

        /// <summary>
        ///     Deserialze a string to an object.
        /// </summary>
        /// <param name="text">The text to deserialize.</param>
        /// <typeparam name="T">The type of object to return.</typeparam>
        /// <returns>The deserialized object.</returns>
        public T Deserialize<T>(string text) => SimpleJson.DeserializeObject<T>(text, this.CreateSerializerStrategy());

        /// <summary>
        ///     Deserialze a string to an object.
        /// </summary>
        /// <param name="text">The text to deserialize.</param>
        /// <param name="type">The type of object to return.</param>
        /// <returns>The deserialized object.</returns>
        public object Deserialize(string text, Type type) => SimpleJson.DeserializeObject(text, type, this.CreateSerializerStrategy());

        /// <summary>
        ///     A method to map property names to output names/
        /// </summary>
        /// <param name="name">The name to map.</param>
        /// <returns>The value.</returns>
        public string MapName(Type parentType, string name) => this.CreateSerializerStrategy().MapName(parentType, name);

        public object DeserializePrimitiveValue(string value, Type valueType)
        {
            var strategy = this.CreateSerializerStrategy();
            return strategy.DeserializeObject(value, valueType);
        }

        public string SerializePrimitiveValue(object value) => SimpleJson.SerializeObject(value, this.CreateSerializerStrategy(), true);

        private CustomPocoJsonSerializerStrategy CreateSerializerStrategy()
        {
            var localConverters = this.converters ?? (serializer => serializer.AllConverters());

            return new CustomPocoJsonSerializerStrategy(
                                                        localConverters(this),
                                                        this.camelCaseOutput,
                                                        this.indented,
                                                        this.augmentClrPropertyName,
                                                        this.newClrPropertyName);
        }
    }
}