﻿namespace Base2art.Serialization
{
    using Converters;

    internal static class StandardConverters
    {
        public static IConverter[] AllConverters(this IJsonSerializer serializer)
        {
            return new IConverter[]
                   {
                       new XElementConverter(),
#if NETSTANDARD2_0
                       new MailAddressConverter(),
#endif
#if !NETSTANDARD1_0
                       new XmlDocumentConverter(),
#endif

                       // CLASSES
                       new DictionaryStringAnyConverter(serializer),
                       new DictionaryStructAnyConverter(serializer),
                       new VersionConverter(),
                       new RegexConverter(serializer),
                       new KeyValuePairConverter(serializer),
                       new ByteArrayConverter(),

                       new TimeSpanConverter(),
                       new NullableStructConverter(new TimeSpanConverter()),

                       new EnumConverter(),
                       new NullableStructConverter(new EnumConverter()),

                       new DateTimeConverter(),
                       new NullableStructConverter(new DateTimeConverter())
                   };
        }
    }
}