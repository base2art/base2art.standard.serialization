﻿namespace Base2art.Serialization.Internals
{
    using System;
    using System.CodeDom.Compiler;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;
    using System.Text;

    [GeneratedCode("simple-json", "1.0.0")]
    internal class PocoJsonSerializerStrategy : IJsonSerializerStrategy
    {
        internal static readonly Type[] EmptyTypes = new Type[0];
        internal static readonly Type[] ArrayConstructorParameterTypes = {typeof(int)};

        private static readonly string[] Iso8601Format =
        {
            @"yyyy-MM-dd\THH:mm:ss.FFFFFFF\Z",
            @"yyyy-MM-dd\THH:mm:ss\Z",
            @"yyyy-MM-dd\THH:mm:ssK"
        };

        internal IDictionary<Type, ReflectionUtils.ConstructorDelegate> ConstructorCache;
        internal IDictionary<Type, IDictionary<string, ReflectionUtils.GetDelegate>> GetCache;
        internal IDictionary<Type, IDictionary<string, KeyValuePair<Type, ReflectionUtils.SetDelegate>>> SetCache;

        public PocoJsonSerializerStrategy()
        {
            this.ConstructorCache =
                new ReflectionUtils.ThreadSafeDictionary<Type, ReflectionUtils.ConstructorDelegate>(this.ContructorDelegateFactory);
            this.GetCache = new ReflectionUtils.ThreadSafeDictionary<Type, IDictionary<string, ReflectionUtils.GetDelegate>>(this.GetterValueFactory);
            this.SetCache =
                new ReflectionUtils.ThreadSafeDictionary<Type, IDictionary<string, KeyValuePair<Type, ReflectionUtils.SetDelegate>>
                >(this.SetterValueFactory);
        }

        public virtual bool TrySerializeNonPrimitiveObject(object input, out object output) =>
            this.TrySerializeKnownTypes(input, out output) || this.TrySerializeUnknownTypes(input, out output);

        [SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        public virtual object DeserializeObject(object value, Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }

            var str = value as string;

            if (type == typeof(Guid) && string.IsNullOrEmpty(str))
            {
                return default(Guid);
            }

            if (value == null)
            {
                return null;
            }

            object obj = null;

            if (str != null)
            {
                if (str.Length != 0) // We know it can't be null now.
                {
                    if (type == typeof(DateTime) || ReflectionUtils.IsNullableType(type) && Nullable.GetUnderlyingType(type) == typeof(DateTime))
                    {
                        return DateTime.ParseExact(str, Iso8601Format, CultureInfo.InvariantCulture,
                                                   DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal);
                    }

                    if (type == typeof(DateTimeOffset)
                        || ReflectionUtils.IsNullableType(type) && Nullable.GetUnderlyingType(type) == typeof(DateTimeOffset))
                    {
                        return DateTimeOffset.ParseExact(str, Iso8601Format, CultureInfo.InvariantCulture,
                                                         DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal);
                    }

                    if (type == typeof(Guid) || ReflectionUtils.IsNullableType(type) && Nullable.GetUnderlyingType(type) == typeof(Guid))
                    {
                        return new Guid(str);
                    }

                    if (type == typeof(Uri))
                    {
                        var isValid = Uri.IsWellFormedUriString(str, UriKind.RelativeOrAbsolute);

                        Uri result;
                        if (isValid && Uri.TryCreate(str, UriKind.RelativeOrAbsolute, out result))
                        {
                            return result;
                        }

                        return null;
                    }

                    if (type == typeof(string))
                    {
                        return str;
                    }

                    return Convert.ChangeType(str, type, CultureInfo.InvariantCulture);
                }

                if (type == typeof(Guid))
                {
                    obj = default(Guid);
                }
                else if (ReflectionUtils.IsNullableType(type) && Nullable.GetUnderlyingType(type) == typeof(Guid))
                {
                    obj = null;
                }
                else
                {
                    obj = str;
                }

                // Empty string case
                if (!ReflectionUtils.IsNullableType(type) && Nullable.GetUnderlyingType(type) == typeof(Guid))
                {
                    return str;
                }
            }
            else if (value is bool)
            {
                return value;
            }

            var valueIsLong = value is long;
            var valueIsDouble = value is double;
            if (valueIsLong && type == typeof(long) || valueIsDouble && type == typeof(double))
            {
                return value;
            }

            if (valueIsDouble && type != typeof(double) || valueIsLong && type != typeof(long))
            {
                obj = type == typeof(int) || type == typeof(long) || type == typeof(double) || type == typeof(float) || type == typeof(bool)
                      || type == typeof(decimal) || type == typeof(byte) || type == typeof(short)
                          ? Convert.ChangeType(value, type, CultureInfo.InvariantCulture)
                          : value;
            }
            else
            {
                var objects = value as IDictionary<string, object>;
                if (objects != null)
                {
                    var jsonObject = objects;

                    if (ReflectionUtils.IsTypeDictionary(type))
                    {
                        // if dictionary then
                        var types = ReflectionUtils.GetGenericTypeArguments(type);
                        var keyType = types[0];
                        var valueType = types[1];

                        var genericType = typeof(Dictionary<,>).MakeGenericType(keyType, valueType);

                        var dict = (IDictionary) this.ConstructorCache[genericType]();

                        foreach (var kvp in jsonObject)
                        {
                            dict.Add(kvp.Key, this.DeserializeObject(kvp.Value, valueType));
                        }

                        obj = dict;
                    }
                    else
                    {
                        if (type == typeof(object))
                        {
                            obj = value;
                        }
                        else
                        {
                            obj = this.ConstructorCache[type]();
                            foreach (var setter in this.SetCache[type])
                            {
                                object jsonValue;
                                if (jsonObject.TryGetValue(setter.Key, out jsonValue))
                                {
                                    jsonValue = this.DeserializeObject(jsonValue, setter.Value.Key);
                                    setter.Value.Value(obj, jsonValue);
                                }
                            }
                        }
                    }
                }
                else
                {
                    var valueAsList = value as IList<object>;
                    if (valueAsList != null)
                    {
                        var jsonObject = valueAsList;
                        IList list = null;

                        if (type.IsArray)
                        {
                            list = (IList) this.ConstructorCache[type](jsonObject.Count);
                            var i = 0;
                            foreach (var o in jsonObject)
                            {
                                list[i++] = this.DeserializeObject(o, type.GetElementType());
                            }
                        }
                        else if (ReflectionUtils.IsTypeGenericeCollectionInterface(type) || ReflectionUtils.IsAssignableFrom(typeof(IList), type))
                        {
                            var innerType = ReflectionUtils.GetGenericListElementType(type);
                            list =
                                (IList) (this.ConstructorCache[type]
                                         ?? this.ConstructorCache[typeof(List<>).MakeGenericType(innerType)])(jsonObject.Count);
                            foreach (var o in jsonObject)
                            {
                                list.Add(this.DeserializeObject(o, innerType));
                            }
                        }

                        obj = list;
                    }
                }

                return obj;
            }

            if (ReflectionUtils.IsNullableType(type))
            {
                return ReflectionUtils.ToNullableType(obj, type);
            }

            return obj;
        }

        public virtual bool CanHandleEnumerableSerialization(Type type, object value) => true;

        public virtual void WriteNameTrivia(StringBuilder builder, int indent)
        {
        }

        public virtual void WriteValueTrivia(StringBuilder builder, int indent)
        {
        }

        public virtual void WriteArrayItemTrivia(StringBuilder builder, int indent)
        {
        }

        public virtual void WriteArrayCloseTrivia(StringBuilder builder, int indent)
        {
        }

        public virtual void WriteObjectCloseTrivia(StringBuilder builder, int indent)
        {
        }

        protected virtual string MapClrMemberNameToJsonFieldName(Type parentType, string clrPropertyName) => clrPropertyName;

        internal virtual ReflectionUtils.ConstructorDelegate ContructorDelegateFactory(Type key) =>
            ReflectionUtils.GetContructor(key, key.IsArray ? ArrayConstructorParameterTypes : EmptyTypes);

        internal virtual IDictionary<string, ReflectionUtils.GetDelegate> GetterValueFactory(Type type)
        {
            IDictionary<string, ReflectionUtils.GetDelegate> result = new Dictionary<string, ReflectionUtils.GetDelegate>();

#if !NETSTANDARD1_0

            if (typeof(DispatchProxy).GetTypeInfo().IsAssignableFrom(type.GetTypeInfo()))
            {
                type = type.GetTypeInfo().ImplementedInterfaces.FirstOrDefault() ?? type;
            }

#endif

            var propertyInfos = ReflectionUtils.GetProperties(type);
            foreach (var propertyInfo in propertyInfos)
            {
                if (propertyInfo.CanRead)
                {
                    var getMethod = ReflectionUtils.GetGetterMethodInfo(propertyInfo);
                    if (getMethod.IsStatic || !getMethod.IsPublic)
                    {
                        continue;
                    }

                    var mapClrMemberNameToJsonFieldName = this.MapClrMemberNameToJsonFieldName(type, propertyInfo.Name);
                    if (!string.IsNullOrWhiteSpace(mapClrMemberNameToJsonFieldName))
                    {
                        result[mapClrMemberNameToJsonFieldName] = ReflectionUtils.GetGetMethod(propertyInfo);
                    }
                }
            }

            var fieldInfos = ReflectionUtils.GetFields(type);
            foreach (var fieldInfo in fieldInfos)
            {
                if (fieldInfo.IsStatic || !fieldInfo.IsPublic)
                {
                    continue;
                }

                var mapClrMemberNameToJsonFieldName = this.MapClrMemberNameToJsonFieldName(type, fieldInfo.Name);
                if (!string.IsNullOrWhiteSpace(mapClrMemberNameToJsonFieldName))
                {
                    result[mapClrMemberNameToJsonFieldName] = ReflectionUtils.GetGetMethod(fieldInfo);
                }
            }

            return result;
        }

        internal virtual IDictionary<string, KeyValuePair<Type, ReflectionUtils.SetDelegate>> SetterValueFactory(Type type)
        {
            IDictionary<string, KeyValuePair<Type, ReflectionUtils.SetDelegate>> result =
                new Dictionary<string, KeyValuePair<Type, ReflectionUtils.SetDelegate>>();
            foreach (var propertyInfo in ReflectionUtils.GetProperties(type))
            {
                if (propertyInfo.CanWrite)
                {
                    var setMethod = ReflectionUtils.GetSetterMethodInfo(propertyInfo);
                    if (setMethod.IsStatic || !setMethod.IsPublic)
                    {
                        continue;
                    }

                    var mapClrMemberNameToJsonFieldName = this.MapClrMemberNameToJsonFieldName(type, propertyInfo.Name);
                    if (!string.IsNullOrWhiteSpace(mapClrMemberNameToJsonFieldName))
                    {
                        result[mapClrMemberNameToJsonFieldName] =
                            new KeyValuePair<Type, ReflectionUtils.SetDelegate>(propertyInfo.PropertyType,
                                                                                ReflectionUtils.GetSetMethod(propertyInfo));
                    }
                }
            }

            foreach (var fieldInfo in ReflectionUtils.GetFields(type))
            {
                if (fieldInfo.IsInitOnly || fieldInfo.IsStatic || !fieldInfo.IsPublic)
                {
                    continue;
                }

                var mapClrMemberNameToJsonFieldName = this.MapClrMemberNameToJsonFieldName(type, fieldInfo.Name);
                if (!string.IsNullOrWhiteSpace(mapClrMemberNameToJsonFieldName))
                {
                    result[mapClrMemberNameToJsonFieldName] =
                        new KeyValuePair<Type, ReflectionUtils.SetDelegate>(fieldInfo.FieldType, ReflectionUtils.GetSetMethod(fieldInfo));
                }
            }

            return result;
        }

        protected virtual object SerializeEnum(Enum p) => Convert.ToDouble(p, CultureInfo.InvariantCulture);

        [SuppressMessage("Microsoft.Design", "CA1007:UseGenericsWhereAppropriate", Justification = "Need to support .NET 2")]
        protected virtual bool TrySerializeKnownTypes(object input, out object output)
        {
            var returnValue = true;
            if (input is DateTime)
            {
                output = ((DateTime) input).ToUniversalTime().ToString(Iso8601Format[0], CultureInfo.InvariantCulture);
            }
            else if (input is DateTimeOffset)
            {
                output = ((DateTimeOffset) input).ToUniversalTime().ToString(Iso8601Format[0], CultureInfo.InvariantCulture);
            }
            else if (input is Guid)
            {
                output = ((Guid) input).ToString("D");
            }
            else if (input is Uri)
            {
                output = input.ToString();
            }
            else
            {
                var inputEnum = input as Enum;
                if (inputEnum != null)
                {
                    output = this.SerializeEnum(inputEnum);
                }
                else
                {
                    returnValue = false;
                    output = null;
                }
            }

            return returnValue;
        }

        [SuppressMessage("Microsoft.Design", "CA1007:UseGenericsWhereAppropriate", Justification = "Need to support .NET 2")]
        protected virtual bool TrySerializeUnknownTypes(object input, out object output)
        {
            if (input == null)
            {
                throw new ArgumentNullException("input");
            }

            output = null;
            var type = input.GetType();
            if (type.FullName == null)
            {
                return false;
            }

            IDictionary<string, object> obj = new JsonObject();
            var getters = this.GetCache[type];
            foreach (var getter in getters)
            {
                if (getter.Value != null)
                {
                    var mapClrMemberNameToJsonFieldName = this.MapClrMemberNameToJsonFieldName(type, getter.Key);
                    if (!string.IsNullOrWhiteSpace(mapClrMemberNameToJsonFieldName))
                    {
                        obj.Add(mapClrMemberNameToJsonFieldName, getter.Value(input));
                    }
                }
            }

            output = obj;
            return true;
        }
    }
}