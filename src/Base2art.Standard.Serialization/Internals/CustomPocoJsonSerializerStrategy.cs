﻿namespace Base2art.Serialization.Internals
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using Converters;

    internal class CustomPocoJsonSerializerStrategy : PocoJsonSerializerStrategy
    {
        private readonly bool camelCase;
        private readonly Func<Type, string, bool> augmentClrPropertyName;
        private readonly Func<Type, string, string> newClrPropertyName;
        private readonly IConverter[] converters;

        public CustomPocoJsonSerializerStrategy(
            IConverter[] converters,
            bool camelCase,
            bool indented,
            Func<Type, string, bool> augmentClrPropertyName,
            Func<Type, string, string> newClrPropertyName)
        {
            this.converters = converters;
            this.camelCase = camelCase;
            this.augmentClrPropertyName = augmentClrPropertyName;
            this.newClrPropertyName = newClrPropertyName;
            this.Indented = indented;
        }

        public bool Indented { get; }

        internal override IDictionary<string, KeyValuePair<Type, ReflectionUtils.SetDelegate>> SetterValueFactory(Type type)
        {
            if (type != null)
            {
                if (type.IsAnonymousType())
                {
                    return ReflectionUtility.GetSetterValueFactoryForAnonymousType(type, this.MapClrMemberNameToJsonFieldName);
                }
            }

            return base.SetterValueFactory(type);
        }

        public override bool CanHandleEnumerableSerialization(Type enumerableValue, object value)
        {
            if (this.converters.Any(x => x.CanSerialize(enumerableValue, value)))
            {
                return false;
            }

            return base.CanHandleEnumerableSerialization(enumerableValue, value);
        }

        internal override ReflectionUtils.ConstructorDelegate ContructorDelegateFactory(Type type)
        {
            if (type != null)
            {
                if (type.IsAnonymousType())
                {
                    return x =>
                    {
                        var ctor = type.GetTypeInfo().DeclaredConstructors.First();

                        var parms = ctor.GetParameters()
                                        .Select(z => z.ParameterType)
                                        .Select(this.GetDefault)
                                        .ToArray();

                        return ctor.Invoke(parms);
                    };
                }
            }

            return base.ContructorDelegateFactory(type);
        }

        protected override object SerializeEnum(Enum p) => p.ToString("G");

        public override object DeserializeObject(object value, Type type)
        {
            foreach (var converter in this.converters)
            {
                if (converter.CanDeserialize(type))
                {
                    return converter.Deserialize(type, value);
                }
            }

            if (type.IsAnonymousType())
            {
                var obj = (JsonObject) value;
                var keys = obj.Keys;

                var ctor = type.GetTypeInfo().DeclaredConstructors.First();

                var parms = ctor.GetParameters()
                                .Select(x => Tuple.Create(this.MapClrMemberNameToJsonFieldName(type, x.Name), x.ParameterType))
                                .Select(x => keys.Contains(x.Item1)
                                                 ? this.DeserializeObject(obj[x.Item1], x.Item2)
                                                 : this.GetDefault(x.Item2))
                                .ToArray();
                return ctor.Invoke(parms);
            }

            if (value is JsonObject o)
            {
                var newObj = new JsonObject(StringComparer.OrdinalIgnoreCase);

                foreach (var key in o.Keys)
                {
                    newObj[key] = o[key];
                }

                value = newObj;
            }
            else if (value is IDictionary<string, object> od)
            {
                value = new Dictionary<string, object>(od, StringComparer.OrdinalIgnoreCase);
            }

            var result = base.DeserializeObject(value, type);

            return result;
        }

        private object GetDefault(Type x) => x.GetTypeInfo().IsValueType
                                                 ? Activator.CreateInstance(x)
                                                 : null;

        public override bool TrySerializeNonPrimitiveObject(object input, out object output)
        {
            if (input != null)
            {
                var objectType = input.GetType();
                foreach (var converter in this.converters)
                {
                    if (converter.CanSerialize(objectType, input))
                    {
                        output = converter.Serialize(objectType, input);

                        return true;
                    }
                }
            }

            return base.TrySerializeNonPrimitiveObject(input, out output);
        }

        public string MapName(Type parentType, string name) => this.MapClrMemberNameToJsonFieldName(parentType, name);

        protected override string MapClrMemberNameToJsonFieldName(Type parentType, string clrPropertyName)
        {
            if ((this.augmentClrPropertyName?.Invoke(parentType, clrPropertyName)).GetValueOrDefault())
            {
                return this.newClrPropertyName?.Invoke(parentType, clrPropertyName);
            }

            var item = base.MapClrMemberNameToJsonFieldName(parentType, clrPropertyName);

            if (!this.camelCase)
            {
                return item;
            }

            return string.IsNullOrWhiteSpace(item) ? item : char.ToLowerInvariant(item[0]) + item.Substring(1);
        }

        public override void WriteValueTrivia(StringBuilder builder, int indent)
        {
            if (this.Indented)
            {
                builder.Append(' ');
            }
            else
            {
                base.WriteNameTrivia(builder, indent);
            }
        }

        public override void WriteNameTrivia(StringBuilder builder, int indent)
        {
            if (this.Indented)
            {
                builder.AppendLine();
                builder.Append(new string(' ', indent * 2));
            }
            else
            {
                base.WriteNameTrivia(builder, indent);
            }
        }

        public override void WriteArrayCloseTrivia(StringBuilder builder, int indent)
        {
            if (this.Indented)
            {
                builder.AppendLine();
                builder.Append(new string(' ', indent * 2));
            }
            else
            {
                base.WriteArrayCloseTrivia(builder, indent);
            }
        }

        public override void WriteArrayItemTrivia(StringBuilder builder, int indent)
        {
            if (this.Indented)
            {
                builder.AppendLine();
                builder.Append(new string(' ', indent * 2));
            }
            else
            {
                base.WriteArrayItemTrivia(builder, indent);
            }
        }

        public override void WriteObjectCloseTrivia(StringBuilder builder, int indent)
        {
            if (this.Indented)
            {
                builder.AppendLine();
                builder.Append(new string(' ', indent * 2));
            }
            else
            {
                base.WriteObjectCloseTrivia(builder, indent);
            }
        }
    }
}