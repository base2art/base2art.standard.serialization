﻿namespace Base2art.Serialization.Internals
{
    using System;
    using System.CodeDom.Compiler;
    using System.Collections;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Reflection;

    [GeneratedCode("reflection-utils", "1.0.0")]
    internal class ReflectionUtils
    {
        public delegate object ConstructorDelegate(params object[] args);

        public delegate object GetDelegate(object source);

        public delegate void SetDelegate(object source, object value);

        public delegate TValue ThreadSafeDictionaryValueFactory<in TKey, out TValue>(TKey key);

        private static readonly object[] EmptyObjects = { };

        public static TypeInfo GetTypeInfo(Type type) => type.GetTypeInfo();

        public static Attribute GetAttribute(MemberInfo info, Type type)
        {
            if (info == null || type == null || !info.IsDefined(type))
            {
                return null;
            }

            return info.GetCustomAttribute(type);
        }

        public static Type GetGenericListElementType(Type type)
        {
            var interfaces = type.GetTypeInfo().ImplementedInterfaces;
            foreach (var implementedInterface in interfaces)
            {
                if (IsTypeGeneric(implementedInterface) &&
                    implementedInterface.GetGenericTypeDefinition() == typeof(IList<>))
                {
                    return GetGenericTypeArguments(implementedInterface)[0];
                }
            }

            return GetGenericTypeArguments(type)[0];
        }

        public static Attribute GetAttribute(Type objectType, Type attributeType)
        {
            if (objectType == null || attributeType == null || !objectType.GetTypeInfo().IsDefined(attributeType))
            {
                return null;
            }

            return objectType.GetTypeInfo().GetCustomAttribute(attributeType);
        }

        public static Type[] GetGenericTypeArguments(Type type) => type.GetTypeInfo().GenericTypeArguments;

        public static bool IsTypeGeneric(Type type) => GetTypeInfo(type).IsGenericType;

        public static bool IsTypeGenericeCollectionInterface(Type type)
        {
            if (!IsTypeGeneric(type))
            {
                return false;
            }

            var genericDefinition = type.GetGenericTypeDefinition();

            return genericDefinition == typeof(IList<>)
                   || genericDefinition == typeof(ICollection<>)
                   || genericDefinition == typeof(IEnumerable<>);
        }

        public static bool IsAssignableFrom(Type type1, Type type2) => GetTypeInfo(type1).IsAssignableFrom(GetTypeInfo(type2));

        public static bool IsTypeDictionary(Type type)
        {
            if (typeof(IDictionary<,>).GetTypeInfo().IsAssignableFrom(type.GetTypeInfo()))
            {
                return true;
            }

            if (!GetTypeInfo(type).IsGenericType)
            {
                return false;
            }

            var genericDefinition = type.GetGenericTypeDefinition();
            return genericDefinition == typeof(IDictionary<,>);
        }

        public static bool IsNullableType(Type type) => GetTypeInfo(type).IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>);

        public static object ToNullableType(object obj, Type nullableType) =>
            obj == null ? null : Convert.ChangeType(obj, Nullable.GetUnderlyingType(nullableType), CultureInfo.InvariantCulture);

        public static bool IsValueType(Type type) => GetTypeInfo(type).IsValueType;

        public static IEnumerable<ConstructorInfo> GetConstructors(Type type) => type.GetTypeInfo().DeclaredConstructors;

        public static ConstructorInfo GetConstructorInfo(Type type, params Type[] argsType)
        {
            var constructorInfos = GetConstructors(type);
            int i;
            bool matches;
            foreach (var constructorInfo in constructorInfos)
            {
                var parameters = constructorInfo.GetParameters();
                if (argsType.Length != parameters.Length)
                {
                    continue;
                }

                i = 0;
                matches = true;
                foreach (var parameterInfo in constructorInfo.GetParameters())
                {
                    if (parameterInfo.ParameterType != argsType[i])
                    {
                        matches = false;
                        break;
                    }
                }

                if (matches)
                {
                    return constructorInfo;
                }
            }

            return null;
        }

        public static IEnumerable<PropertyInfo> GetProperties(Type type) => type.GetRuntimeProperties();

        public static IEnumerable<FieldInfo> GetFields(Type type) => type.GetRuntimeFields();

        public static MethodInfo GetGetterMethodInfo(PropertyInfo propertyInfo) => propertyInfo.GetMethod;

        public static MethodInfo GetSetterMethodInfo(PropertyInfo propertyInfo) => propertyInfo.SetMethod;

        public static ConstructorDelegate GetContructor(Type type, params Type[] argsType) => GetConstructorByExpression(type, argsType);

        public static ConstructorDelegate GetConstructorByReflection(ConstructorInfo constructorInfo)
        {
            return delegate(object[] args) { return constructorInfo.Invoke(args); };
        }

        public static ConstructorDelegate GetConstructorByExpression(ConstructorInfo constructorInfo)
        {
            var paramsInfo = constructorInfo.GetParameters();
            var param = Expression.Parameter(typeof(object[]), "args");
            var argsExp = new Expression[paramsInfo.Length];
            for (var i = 0; i < paramsInfo.Length; i++)
            {
                Expression index = Expression.Constant(i);
                var paramType = paramsInfo[i].ParameterType;
                Expression paramAccessorExp = Expression.ArrayIndex(param, index);
                Expression paramCastExp = Expression.Convert(paramAccessorExp, paramType);
                argsExp[i] = paramCastExp;
            }

            var newExp = Expression.New(constructorInfo, argsExp);
            var lambda = Expression.Lambda<Func<object[], object>>(newExp, param);
            var compiledLambda = lambda.Compile();
            return delegate(object[] args) { return compiledLambda(args); };
        }

        public static ConstructorDelegate GetConstructorByExpression(Type type, params Type[] argsType)
        {
            var constructorInfo = GetConstructorInfo(type, argsType);
            return constructorInfo == null ? null : GetConstructorByExpression(constructorInfo);
        }

        public static GetDelegate GetGetMethod(PropertyInfo propertyInfo) => GetGetMethodByExpression(propertyInfo);

        public static GetDelegate GetGetMethod(FieldInfo fieldInfo) => GetGetMethodByExpression(fieldInfo);

        public static GetDelegate GetGetMethodByReflection(FieldInfo fieldInfo)
        {
            return delegate(object source) { return fieldInfo.GetValue(source); };
        }

        public static GetDelegate GetGetMethodByExpression(PropertyInfo propertyInfo)
        {
            var getMethodInfo = GetGetterMethodInfo(propertyInfo);
            var instance = Expression.Parameter(typeof(object), "instance");
            var instanceCast = !IsValueType(propertyInfo.DeclaringType)
                                   ? Expression.TypeAs(instance, propertyInfo.DeclaringType)
                                   : Expression.Convert(instance, propertyInfo.DeclaringType);
            var compiled = Expression
                           .Lambda<Func<object, object>>(Expression.TypeAs(Expression.Call(instanceCast, getMethodInfo), typeof(object)),
                                                         instance)
                           .Compile();
            return delegate(object source) { return compiled(source); };
        }

        public static GetDelegate GetGetMethodByExpression(FieldInfo fieldInfo)
        {
            var instance = Expression.Parameter(typeof(object), "instance");
            var member = Expression.Field(Expression.Convert(instance, fieldInfo.DeclaringType), fieldInfo);
            var compiled = Expression.Lambda<GetDelegate>(Expression.Convert(member, typeof(object)), instance).Compile();
            return delegate(object source) { return compiled(source); };
        }

        public static SetDelegate GetSetMethod(PropertyInfo propertyInfo) => GetSetMethodByExpression(propertyInfo);

        public static SetDelegate GetSetMethod(FieldInfo fieldInfo) => GetSetMethodByExpression(fieldInfo);

        public static SetDelegate GetSetMethodByExpression(PropertyInfo propertyInfo)
        {
            var setMethodInfo = GetSetterMethodInfo(propertyInfo);
            var instance = Expression.Parameter(typeof(object), "instance");
            var value = Expression.Parameter(typeof(object), "value");
            var instanceCast = !IsValueType(propertyInfo.DeclaringType)
                                   ? Expression.TypeAs(instance, propertyInfo.DeclaringType)
                                   : Expression.Convert(instance, propertyInfo.DeclaringType);
            var valueCast = !IsValueType(propertyInfo.PropertyType)
                                ? Expression.TypeAs(value, propertyInfo.PropertyType)
                                : Expression.Convert(value, propertyInfo.PropertyType);
            var compiled = Expression.Lambda<Action<object, object>>(Expression.Call(instanceCast, setMethodInfo, valueCast), instance, value)
                                     .Compile();
            return delegate(object source, object val) { compiled(source, val); };
        }

        public static SetDelegate GetSetMethodByExpression(FieldInfo fieldInfo)
        {
            var instance = Expression.Parameter(typeof(object), "instance");
            var value = Expression.Parameter(typeof(object), "value");
            var compiled = Expression.Lambda<Action<object, object>>(
                                                                     Assign(Expression.Field(Expression.Convert(instance, fieldInfo.DeclaringType), fieldInfo),
                                                                            Expression.Convert(value, fieldInfo.FieldType)), instance, value)
                                     .Compile();
            return delegate(object source, object val) { compiled(source, val); };
        }

        public static BinaryExpression Assign(Expression left, Expression right) => Expression.Assign(left, right);

        public sealed class ThreadSafeDictionary<TKey, TValue> : IDictionary<TKey, TValue>
        {
            private readonly object _lock = new object();
            private readonly ThreadSafeDictionaryValueFactory<TKey, TValue> valueFactory;
            private Dictionary<TKey, TValue> dictionary;

            public ThreadSafeDictionary(ThreadSafeDictionaryValueFactory<TKey, TValue> valueFactory) => this.valueFactory = valueFactory;

            public void Add(TKey key, TValue value)
            {
                throw new NotImplementedException();
            }

            public bool ContainsKey(TKey key) => this.dictionary.ContainsKey(key);

            public ICollection<TKey> Keys => this.dictionary.Keys;

            public bool Remove(TKey key) => throw new NotImplementedException();

            public bool TryGetValue(TKey key, out TValue value)
            {
                value = this[key];
                return true;
            }

            public ICollection<TValue> Values => this.dictionary.Values;

            public TValue this[TKey key]
            {
                get => this.Get(key);
                set => throw new NotImplementedException();
            }

            public void Add(KeyValuePair<TKey, TValue> item)
            {
                throw new NotImplementedException();
            }

            public void Clear()
            {
                throw new NotImplementedException();
            }

            public bool Contains(KeyValuePair<TKey, TValue> item) => throw new NotImplementedException();

            public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
            {
                throw new NotImplementedException();
            }

            public int Count => this.dictionary.Count;
            public bool IsReadOnly => throw new NotImplementedException();

            public bool Remove(KeyValuePair<TKey, TValue> item) => throw new NotImplementedException();

            public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() => this.dictionary.GetEnumerator();

            IEnumerator IEnumerable.GetEnumerator() => this.dictionary.GetEnumerator();

            private TValue Get(TKey key)
            {
                if (this.dictionary == null)
                {
                    return this.AddValue(key);
                }

                if (!this.dictionary.TryGetValue(key, out var value))
                {
                    return this.AddValue(key);
                }

                return value;
            }

            private TValue AddValue(TKey key)
            {
                var value = this.valueFactory(key);
                lock (this._lock)
                {
                    if (this.dictionary == null)
                    {
                        this.dictionary = new Dictionary<TKey, TValue>();
                        this.dictionary[key] = value;
                    }
                    else
                    {
                        if (this.dictionary.TryGetValue(key, out var val))
                        {
                            return val;
                        }

                        var dict = new Dictionary<TKey, TValue>(this.dictionary);
                        dict[key] = value;
                        this.dictionary = dict;
                    }
                }

                return value;
            }
        }
    }
}