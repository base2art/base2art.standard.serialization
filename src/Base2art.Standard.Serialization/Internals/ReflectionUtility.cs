﻿namespace Base2art.Serialization.Internals
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.CompilerServices;

    internal static class ReflectionUtility
    {
        public static bool IsNullable(this Type type) => type.GetTypeInfo().IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>);

        public static bool IsAnonymousType(this Type type)
        {
            bool HasCompilerGeneratedAttribute() => type.GetTypeInfo().GetCustomAttributes(typeof(CompilerGeneratedAttribute), false).Any();

            bool NameContainsAnonymousType() => type.FullName.Contains("AnonymousType");

            var isAnonymousType = NameContainsAnonymousType() && HasCompilerGeneratedAttribute();

            return isAnonymousType;
        }

        internal static IDictionary<string, KeyValuePair<Type, ReflectionUtils.SetDelegate>> GetSetterValueFactoryForAnonymousType(
            Type type,
            Func<Type, string, string> strategy)
        {
            var result = new Dictionary<string, KeyValuePair<Type, ReflectionUtils.SetDelegate>>();

            foreach (var propertyInfo in type.GetProperties())
            {
                result[strategy(type, propertyInfo.Name)] =
                    new KeyValuePair<Type, ReflectionUtils.SetDelegate>(propertyInfo.PropertyType, ReflectionUtils.GetSetMethod(propertyInfo));
            }

            return result;
        }

        private static IEnumerable<PropertyInfo> GetProperties(this Type type)
        {
            var props = type.GetRuntimeProperties();
            return props;
        }
    }
}