﻿namespace Base2art.Serialization.Internals
{
    using System;
    using System.CodeDom.Compiler;
    using System.Diagnostics.CodeAnalysis;
    using System.Text;

    [GeneratedCode("simple-json", "1.0.0")]
    internal interface IJsonSerializerStrategy
    {
        [SuppressMessage("Microsoft.Design", "CA1007:UseGenericsWhereAppropriate", Justification = "Need to support .NET 2")]
        bool TrySerializeNonPrimitiveObject(object input, out object output);

        object DeserializeObject(object value, Type type);

        bool CanHandleEnumerableSerialization(Type type, object value);

        void WriteNameTrivia(StringBuilder builder, int indent);

        void WriteValueTrivia(StringBuilder builder, int indent);

        void WriteArrayItemTrivia(StringBuilder builder, int indent);

        void WriteArrayCloseTrivia(StringBuilder builder, int indent);

        void WriteObjectCloseTrivia(StringBuilder builder, int indent);
    }
}