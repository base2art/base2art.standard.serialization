﻿//-----------------------------------------------------------------------
// <copyright file="SimpleJson.cs" company="The Outercurve Foundation">
//    Copyright (c) 2011, The Outercurve Foundation.
//
//    Licensed under the MIT License (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//      http://www.opensource.org/licenses/mit-license.php
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
// </copyright>
// <author>Nathan Totten (ntotten.com), Jim Zimmerman (jimzimmerman.com) and Prabir Shrestha (prabir.me)</author>
// <website>https://github.com/facebook-csharp-sdk/simple-json</website>
//-----------------------------------------------------------------------

// VERSION: 0.38.0

// NOTE: uncomment the following line to make SimpleJson class internal.

//#define SIMPLE_JSON_INTERNAL

// NOTE: uncomment the following line to make JsonArray and JsonObject class internal.
//#define SIMPLE_JSON_OBJARRAYINTERNAL

// NOTE: uncomment the following line to enable dynamic support.
//#define SIMPLE_JSON_DYNAMIC

// NOTE: uncomment the following line to enable IReadOnlyCollection<T> and IReadOnlyList<T> support.
//#define SIMPLE_JSON_READONLY_COLLECTIONS

// NOTE: uncomment the following line to disable linq expressions/compiled lambda (better performance) instead of method.invoke().
// define if you are using .net framework <= 3.0 or < WP7.5

// NOTE: uncomment the following line if you are compiling under Window Metro style application/library.
// usually already defined in properties
//#define NETFX_CORE

// If you are targetting WinStore, WP8 and NET4.5+ PCL make sure to #define SIMPLE_JSON_TYPEINFO;

// original json parsing code from http://techblog.procurios.nl/k/618/news/view/14605/14863/How-do-I-write-my-own-parser-for-JSON.html

//using System.Runtime.Serialization;

// ReSharper disable LoopCanBeConvertedToQuery
// ReSharper disable RedundantExplicitArrayCreation
// ReSharper disable SuggestUseVarKeywordEvident

namespace Base2art.Serialization.Internals
{
    using System;
    using System.CodeDom.Compiler;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.Linq;
    using System.Text;

    /// <summary>
    ///     This class encodes and decodes JSON strings.
    ///     Spec. details, see http://www.json.org/
    ///     JSON uses Arrays and Objects. These correspond here to the datatypes JsonArray(IList&lt;object>) and
    ///     JsonObject(IDictionary&lt;string,object>).
    ///     All numbers are parsed to doubles.
    /// </summary>
    [GeneratedCode("simple-json", "1.0.0")]
    internal static class SimpleJson
    {
        private const int TOKEN_NONE = 0;
        private const int TOKEN_CURLY_OPEN = 1;
        private const int TOKEN_CURLY_CLOSE = 2;
        private const int TOKEN_SQUARED_OPEN = 3;
        private const int TOKEN_SQUARED_CLOSE = 4;
        private const int TOKEN_COLON = 5;
        private const int TOKEN_COMMA = 6;
        private const int TOKEN_STRING = 7;
        private const int TOKEN_NUMBER = 8;
        private const int TOKEN_TRUE = 9;
        private const int TOKEN_FALSE = 10;
        private const int TOKEN_NULL = 11;
        private const int BUILDER_CAPACITY = 2000;

        private static readonly char[] EscapeTable;
        private static readonly char[] EscapeCharacters = {'"', '\\', '\b', '\f', '\n', '\r', '\t'};
        private static readonly string EscapeCharactersString = new string(EscapeCharacters);

        static SimpleJson()
        {
            EscapeTable = new char[93];
            EscapeTable['"'] = '"';
            EscapeTable['\\'] = '\\';
            EscapeTable['\b'] = 'b';
            EscapeTable['\f'] = 'f';
            EscapeTable['\n'] = 'n';
            EscapeTable['\r'] = 'r';
            EscapeTable['\t'] = 't';
        }

        /// <summary>
        ///     Parses the string json into a value
        /// </summary>
        /// <param name="json">A JSON string.</param>
        /// <returns>An IList&lt;object>, a IDictionary&lt;string,object>, a double, a string, null, true, or false</returns>
        public static object DeserializeObject(string json)
        {
            object obj;
            if (TryDeserializeObject(json, out obj))
            {
                return obj;
            }

            throw new SerializationException("Invalid JSON string");
        }

        /// <summary>
        ///     Try parsing the json string into a value.
        /// </summary>
        /// <param name="json">
        ///     A JSON string.
        /// </param>
        /// <param name="obj">
        ///     The object.
        /// </param>
        /// <returns>
        ///     Returns true if successfull otherwise false.
        /// </returns>
        [SuppressMessage("Microsoft.Design", "CA1007:UseGenericsWhereAppropriate", Justification = "Need to support .NET 2")]
        public static bool TryDeserializeObject(string json, out object obj)
        {
            var success = true;
            if (json != null)
            {
                var charArray = json.ToCharArray();
                var index = 0;
                obj = ParseValue(charArray, ref index, ref success);
            }
            else
            {
                obj = null;
            }

            return success;
        }

        public static object DeserializeObject(string json, Type type, IJsonSerializerStrategy jsonSerializerStrategy)
        {
            if (jsonSerializerStrategy == null)
            {
                throw new ArgumentNullException(nameof(jsonSerializerStrategy));
            }

            var jsonObject = DeserializeObject(json);
            return type == null || jsonObject != null && ReflectionUtils.IsAssignableFrom(jsonObject.GetType(), type)
                       ? jsonObject
                       : jsonSerializerStrategy.DeserializeObject(jsonObject, type);
        }

//        public static object DeserializeObject(string json, Type type) => DeserializeObject(json, type, null);

        public static T DeserializeObject<T>(string json, IJsonSerializerStrategy jsonSerializerStrategy)
        {
            var conversionType = typeof(T);
            var result = DeserializeObject(json, conversionType, jsonSerializerStrategy);
            return (T) result;
        }

//        public static T DeserializeObject<T>(string json, IJsonSerializer p) => (T) DeserializeObject(json, typeof(T), null);

        /// <summary>
        ///     Converts a IDictionary&lt;string,object> / IList&lt;object> object into a JSON string
        /// </summary>
        /// <param name="json">A IDictionary&lt;string,object> / IList&lt;object></param>
        /// <param name="jsonSerializerStrategy">Serializer strategy to use</param>
        /// <returns>A JSON encoded string, or null if object 'json' is not serializable</returns>
        public static string SerializeObject(object json, IJsonSerializerStrategy jsonSerializerStrategy, bool excludeWrapper = false)
        {
            var builder = new StringBuilder(BUILDER_CAPACITY);
            var success = SerializeValue(jsonSerializerStrategy, json, builder, excludeWrapper, 0);
            return success ? builder.ToString() : null;
        }

//        public static string SerializeObject(object json) => SerializeObject(json, new PocoJsonSerializerStrategy());

        public static string EscapeToJavascriptString(string jsonString)
        {
            if (string.IsNullOrEmpty(jsonString))
            {
                return jsonString;
            }

            var sb = new StringBuilder();
            char c;

            for (var i = 0; i < jsonString.Length;)
            {
                c = jsonString[i++];

                if (c == '\\')
                {
                    var remainingLength = jsonString.Length - i;
                    if (remainingLength >= 2)
                    {
                        var lookahead = jsonString[i];
                        if (lookahead == '\\')
                        {
                            sb.Append('\\');
                            ++i;
                        }
                        else if (lookahead == '"')
                        {
                            sb.Append("\"");
                            ++i;
                        }
                        else if (lookahead == 't')
                        {
                            sb.Append('\t');
                            ++i;
                        }
                        else if (lookahead == 'b')
                        {
                            sb.Append('\b');
                            ++i;
                        }
                        else if (lookahead == 'n')
                        {
                            sb.Append('\n');
                            ++i;
                        }
                        else if (lookahead == 'r')
                        {
                            sb.Append('\r');
                            ++i;
                        }
                    }
                }
                else
                {
                    sb.Append(c);
                }
            }

            return sb.ToString();
        }

        private static IDictionary<string, object> ParseObject(char[] json, ref int index, ref bool success)
        {
            IDictionary<string, object> table = new JsonObject();
            int token;

            // {
            NextToken(json, ref index, out _);

            var done = false;
            while (!done)
            {
                token = LookAhead(json, index, out var delimiter);

                var offset = 0;
                var isName = false;
                if (token == TOKEN_NONE)
                {
                    delimiter = new[] {':', ' ', '\b', '\f', '\n', '\r', '\t'};
                    token = TOKEN_STRING;
                    offset = -1;
                    isName = true;
//                    success = false;
//                    return null;
                }

                if (token == TOKEN_COMMA)
                {
                    NextToken(json, ref index, out _);
                }
                else if (token == TOKEN_CURLY_CLOSE)
                {
                    NextToken(json, ref index, out _);
                    return table;
                }
                else
                {
                    // name
                    index += offset;
                    var name = ParseString(json, ref index, ref success, delimiter, isName);
                    index += offset;
                    if (!success)
                    {
                        success = false;
                        return null;
                    }

                    // :
                    token = NextToken(json, ref index, out _);
                    if (token != TOKEN_COLON)
                    {
                        success = false;
                        return null;
                    }

                    // value
                    var value = ParseValue(json, ref index, ref success);
                    if (!success)
                    {
                        success = false;
                        return null;
                    }

                    table[name] = value;
                }
            }

            return table;
        }

        private static JsonArray ParseArray(char[] json, ref int index, ref bool success)
        {
            var array = new JsonArray();

            // [
            NextToken(json, ref index, out _);

            var done = false;
            while (!done)
            {
                var token = LookAhead(json, index, out _);
                if (token == TOKEN_NONE)
                {
                    success = false;
                    return null;
                }

                if (token == TOKEN_COMMA)
                {
                    NextToken(json, ref index, out _);
                }
                else if (token == TOKEN_SQUARED_CLOSE)
                {
                    NextToken(json, ref index, out _);
                    break;
                }
                else
                {
                    var value = ParseValue(json, ref index, ref success);
                    if (!success)
                    {
                        return null;
                    }

                    array.Add(value);
                }
            }

            return array;
        }

        private static object ParseValue(char[] json, ref int index, ref bool success)
        {
            switch (LookAhead(json, index, out var token))
            {
                case TOKEN_STRING:
                    // '"'
                    var delimiter = token;
                    return ParseString(json, ref index, ref success, delimiter, false);
                case TOKEN_NUMBER:
                    return ParseNumber(json, ref index, ref success);
                case TOKEN_CURLY_OPEN:
                    return ParseObject(json, ref index, ref success);
                case TOKEN_SQUARED_OPEN:
                    return ParseArray(json, ref index, ref success);
                case TOKEN_TRUE:
                    NextToken(json, ref index, out _);
                    return true;
                case TOKEN_FALSE:
                    NextToken(json, ref index, out _);
                    return false;
                case TOKEN_NULL:
                    NextToken(json, ref index, out _);
                    return null;
                case TOKEN_NONE:
                    break;
            }

            success = false;
            return null;
        }

        private static string ParseString(char[] json, ref int index, ref bool success, char[] delimiter, bool isName)
        {
            var s = new StringBuilder(BUILDER_CAPACITY);
            char c;

            EatWhitespace(json, ref index);

            // "
            c = json[index++];
            var complete = false;
            while (!complete)
            {
                if (index == json.Length)
                {
                    break;
                }

                c = json[index++];
                if (delimiter.Any(x => x == c))
                {
                    complete = true;
                    break;
                }

                if (c == '\\')
                {
                    if (isName)
                    {
                        throw new SerializationException("invalid character in name");
                    }

                    if (index == json.Length)
                    {
                        break;
                    }

                    c = json[index++];
                    if (delimiter.Any(x => x == c))
                    {
                        s.Append(delimiter);
                    }
                    else if (c == '\\')
                    {
                        s.Append('\\');
                    }
                    else if (c == '/')
                    {
                        s.Append('/');
                    }
                    else if (c == 'b')
                    {
                        s.Append('\b');
                    }
                    else if (c == 'f')
                    {
                        s.Append('\f');
                    }
                    else if (c == 'n')
                    {
                        s.Append('\n');
                    }
                    else if (c == 'r')
                    {
                        s.Append('\r');
                    }
                    else if (c == 't')
                    {
                        s.Append('\t');
                    }
                    else if (c == 'u')
                    {
                        var remainingLength = json.Length - index;
                        if (remainingLength >= 4)
                        {
                            // parse the 32 bit hex into an integer codepoint
                            uint codePoint;
                            if (!(success = uint.TryParse(new string(json, index, 4), NumberStyles.HexNumber, CultureInfo.InvariantCulture,
                                                          out codePoint)))
                            {
                                return "";
                            }

                            // convert the integer codepoint to a unicode char and add to string
                            if (0xD800 <= codePoint && codePoint <= 0xDBFF) // if high surrogate
                            {
                                index += 4; // skip 4 chars
                                remainingLength = json.Length - index;
                                if (remainingLength >= 6)
                                {
                                    uint lowCodePoint;
                                    if (new string(json, index, 2) == "\\u"
                                        && uint.TryParse(new string(json, index + 2, 4), NumberStyles.HexNumber, CultureInfo.InvariantCulture,
                                                         out lowCodePoint))
                                    {
                                        if (0xDC00 <= lowCodePoint && lowCodePoint <= 0xDFFF) // if low surrogate
                                        {
                                            s.Append((char) codePoint);
                                            s.Append((char) lowCodePoint);
                                            index += 6; // skip 6 chars
                                            continue;
                                        }
                                    }
                                }

                                success = false; // invalid surrogate pair
                                return "";
                            }

                            s.Append(ConvertFromUtf32((int) codePoint));
                            // skip 4 chars
                            index += 4;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                else
                {
                    if (isName && !(char.IsLetterOrDigit(c) || c == '$' || c == '_'))
                    {
                        throw new SerializationException("invalid character in name");
                    }

                    s.Append(c);
                }
            }

            if (!complete)
            {
                success = false;
                return null;
            }

            return s.ToString();
        }

        private static string ConvertFromUtf32(int utf32)
        {
            // http://www.java2s.com/Open-Source/CSharp/2.6.4-mono-.net-core/System/System/Char.cs.htm
            if (utf32 < 0 || utf32 > 0x10FFFF)
            {
                throw new ArgumentOutOfRangeException("utf32", "The argument must be from 0 to 0x10FFFF.");
            }

            if (0xD800 <= utf32 && utf32 <= 0xDFFF)
            {
                throw new ArgumentOutOfRangeException("utf32", "The argument must not be in surrogate pair range.");
            }

            if (utf32 < 0x10000)
            {
                return new string((char) utf32, 1);
            }

            utf32 -= 0x10000;
            return new string(new char[] {(char) ((utf32 >> 10) + 0xD800), (char) (utf32 % 0x0400 + 0xDC00)});
        }

        private static object ParseNumber(char[] json, ref int index, ref bool success)
        {
            EatWhitespace(json, ref index);
            var lastIndex = GetLastIndexOfNumber(json, index);
            var charLength = lastIndex - index + 1;
            object returnNumber;
            var str = new string(json, index, charLength);
            if (str.IndexOf(".", StringComparison.OrdinalIgnoreCase) != -1 || str.IndexOf("e", StringComparison.OrdinalIgnoreCase) != -1)
            {
                double number;
                success = double.TryParse(new string(json, index, charLength), NumberStyles.Any, CultureInfo.InvariantCulture, out number);
                returnNumber = number;
            }
            else
            {
                long number;
                success = long.TryParse(new string(json, index, charLength), NumberStyles.Any, CultureInfo.InvariantCulture, out number);
                returnNumber = number;
            }

            index = lastIndex + 1;
            return returnNumber;
        }

        private static int GetLastIndexOfNumber(char[] json, int index)
        {
            int lastIndex;
            for (lastIndex = index; lastIndex < json.Length; lastIndex++)
            {
                if ("0123456789+-.eE".IndexOf(json[lastIndex]) == -1)
                {
                    break;
                }
            }

            return lastIndex - 1;
        }

        private static void EatWhitespace(char[] json, ref int index)
        {
            for (; index < json.Length; index++)
            {
                if (" \t\n\r\b\f".IndexOf(json[index]) == -1)
                {
                    break;
                }
            }
        }

        private static int LookAhead(char[] json, int index, out char[] token)
        {
            var saveIndex = index;
            return NextToken(json, ref saveIndex, out token);
        }

        [SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        private static int NextToken(char[] json, ref int index, out char[] token)
        {
            EatWhitespace(json, ref index);
            if (index == json.Length)
            {
                token = new char[0];
                return TOKEN_NONE;
            }

            var c = json[index];
            index++;
            token = new[] {c};
            switch (c)
            {
                case '{':
                    return TOKEN_CURLY_OPEN;
                case '}':
                    return TOKEN_CURLY_CLOSE;
                case '[':
                    return TOKEN_SQUARED_OPEN;
                case ']':
                    return TOKEN_SQUARED_CLOSE;
                case ',':
                    return TOKEN_COMMA;
                case '"':
                case '\'':
                    return TOKEN_STRING;
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                case '-':
                    return TOKEN_NUMBER;
                case ':':
                    return TOKEN_COLON;
            }

            index--;
            var remainingLength = json.Length - index;
            // false
            if (remainingLength >= 5)
            {
                if (json[index] == 'f' && json[index + 1] == 'a' && json[index + 2] == 'l' && json[index + 3] == 's' && json[index + 4] == 'e')
                {
                    index += 5;
                    token = new char[0];
                    return TOKEN_FALSE;
                }
            }

            // true
            if (remainingLength >= 4)
            {
                if (json[index] == 't' && json[index + 1] == 'r' && json[index + 2] == 'u' && json[index + 3] == 'e')
                {
                    index += 4;
                    token = new char[0];
                    return TOKEN_TRUE;
                }
            }

            // null
            if (remainingLength >= 4)
            {
                if (json[index] == 'n' && json[index + 1] == 'u' && json[index + 2] == 'l' && json[index + 3] == 'l')
                {
                    index += 4;
                    token = new char[0];
                    return TOKEN_NULL;
                }
            }

            token = new char[0];
            return TOKEN_NONE;
        }

        private static bool SerializeValue(
            IJsonSerializerStrategy jsonSerializerStrategy,
            object value,
            StringBuilder builder,
            bool excludeWrapper,
            int indent)
        {
            var success = true;
            var stringValue = value as string;
            if (stringValue != null)
            {
                success = SerializeString(stringValue, builder, excludeWrapper);
            }
            else
            {
                var dict = value as IDictionary<string, object>;
                if (dict != null)
                {
                    success = SerializeObject(jsonSerializerStrategy, dict.Keys, dict.Values, builder, indent);
                }
                else
                {
                    var stringDictionary = value as IDictionary<string, string>;
                    if (stringDictionary != null)
                    {
                        success = SerializeObject(jsonSerializerStrategy, stringDictionary.Keys, stringDictionary.Values, builder, indent);
                    }
                    else
                    {
                        var enumerableValue = value as IEnumerable;
                        if (enumerableValue != null
                            && jsonSerializerStrategy.CanHandleEnumerableSerialization(enumerableValue.GetType(), enumerableValue))
                        {
                            success = SerializeArray(jsonSerializerStrategy, enumerableValue, builder, indent);
                        }
                        else if (IsNumeric(value))
                        {
                            success = SerializeNumber(value, builder);
                        }
                        else if (value is bool)
                        {
                            builder.Append((bool) value ? "true" : "false");
                        }
                        else if (value == null)
                        {
                            builder.Append("null");
                        }
                        else
                        {
                            object serializedObject;
                            success = jsonSerializerStrategy.TrySerializeNonPrimitiveObject(value, out serializedObject);

                            if (success)
                            {
                                SerializeValue(jsonSerializerStrategy, serializedObject, builder, excludeWrapper, indent);
                            }
                        }
                    }
                }
            }

            return success;
        }

        private static bool SerializeObject(
            IJsonSerializerStrategy jsonSerializerStrategy,
            IEnumerable keys,
            IEnumerable values,
            StringBuilder builder,
            int indent)
        {
            builder.Append("{");
            var ke = keys.GetEnumerator();
            var ve = values.GetEnumerator();
            var first = true;
            while (ke.MoveNext() && ve.MoveNext())
            {
                var key = ke.Current;
                var value = ve.Current;
                if (!first)
                {
                    builder.Append(",");
                }

                var stringKey = key as string;
                if (stringKey != null)
                {
                    jsonSerializerStrategy.WriteNameTrivia(builder, indent + 1);

                    SerializeString(stringKey, builder, false);
                }
                else if (!SerializeValue(jsonSerializerStrategy, value, builder, false, indent + 1))
                {
                    return false;
                }

                builder.Append(":");
                jsonSerializerStrategy.WriteValueTrivia(builder, indent + 1);
                if (!SerializeValue(jsonSerializerStrategy, value, builder, false, indent + 1))
                {
                    return false;
                }

                first = false;
            }

            jsonSerializerStrategy.WriteObjectCloseTrivia(builder, indent);
            builder.Append("}");
            return true;
        }

        private static bool SerializeArray(IJsonSerializerStrategy jsonSerializerStrategy, IEnumerable anArray, StringBuilder builder, int indent)
        {
            builder.Append("[");
            var first = true;
            foreach (var value in anArray)
            {
                if (!first)
                {
                    builder.Append(",");
                }

                jsonSerializerStrategy.WriteArrayItemTrivia(builder, indent + 1);

                if (!SerializeValue(jsonSerializerStrategy, value, builder, false, indent + 1))
                {
                    return false;
                }

                first = false;
            }

            jsonSerializerStrategy.WriteArrayCloseTrivia(builder, indent);
            builder.Append("]");
            return true;
        }

        private static bool SerializeString(string aString, StringBuilder builder, bool excludeWrapper)
        {
            // Happy path if there's nothing to be escaped. IndexOfAny is highly optimized (and unmanaged)
            if (aString.IndexOfAny(EscapeCharacters) == -1)
            {
                if (excludeWrapper)
                {
                    builder.Append(aString);
                }
                else
                {
                    builder.Append('"');
                    builder.Append(aString);
                    builder.Append('"');
                }

                return true;
            }

            if (!excludeWrapper)
            {
                builder.Append('"');
            }

            var safeCharacterCount = 0;
            var charArray = aString.ToCharArray();

            for (var i = 0; i < charArray.Length; i++)
            {
                var c = charArray[i];

                // Non ascii characters are fine, buffer them up and send them to the builder
                // in larger chunks if possible. The escape table is a 1:1 translation table
                // with \0 [default(char)] denoting a safe character.
                if (c >= EscapeTable.Length || EscapeTable[c] == default(char))
                {
                    safeCharacterCount++;
                }
                else
                {
                    if (safeCharacterCount > 0)
                    {
                        builder.Append(charArray, i - safeCharacterCount, safeCharacterCount);
                        safeCharacterCount = 0;
                    }

                    builder.Append('\\');
                    builder.Append(EscapeTable[c]);
                }
            }

            if (safeCharacterCount > 0)
            {
                builder.Append(charArray, charArray.Length - safeCharacterCount, safeCharacterCount);
            }

            if (!excludeWrapper)
            {
                builder.Append('"');
            }

            return true;
        }

        private static bool SerializeNumber(object number, StringBuilder builder)
        {
            if (number is long)
            {
                builder.Append(((long) number).ToString(CultureInfo.InvariantCulture));
            }
            else if (number is ulong)
            {
                builder.Append(((ulong) number).ToString(CultureInfo.InvariantCulture));
            }
            else if (number is int)
            {
                builder.Append(((int) number).ToString(CultureInfo.InvariantCulture));
            }
            else if (number is uint)
            {
                builder.Append(((uint) number).ToString(CultureInfo.InvariantCulture));
            }
            else if (number is decimal)
            {
                builder.Append(((decimal) number).ToString(CultureInfo.InvariantCulture));
            }
            else if (number is float)
            {
                builder.Append(((float) number).ToString(CultureInfo.InvariantCulture));
            }
            else
            {
                builder.Append(Convert.ToDouble(number, CultureInfo.InvariantCulture).ToString("r", CultureInfo.InvariantCulture));
            }

            return true;
        }

        /// <summary>
        ///     Determines if a given object is numeric in any way
        ///     (can be integer, double, null, etc).
        /// </summary>
        private static bool IsNumeric(object value)
        {
            switch (value)
            {
                case sbyte _:
                case byte _:
                case short _:
                case ushort _:
                case int _:
                case uint _:
                case long _:
                case ulong _:
                case float _:
                case double _:
                case decimal _:
                    return true;
            }

            return false;
        }
    }
}
// ReSharper restore LoopCanBeConvertedToQuery
// ReSharper restore RedundantExplicitArrayCreation
// ReSharper restore SuggestUseVarKeywordEvident