﻿namespace Base2art.Serialization
{
    using System;
    using Internals;

    /// <summary>
    ///     A simple serializer that camelCases the serialised output.
    /// </summary>
    public sealed class CamelCasingSimpleJsonSerializer : IJsonSerializer
    {
        /// <summary>
        ///     Serialize an object to a string.
        /// </summary>
        /// <param name="item">The item to serialize.</param>
        /// <typeparam name="T">The type of object to serialize.</typeparam>
        /// <returns>The serialized value.</returns>
        public string Serialize<T>(T item) => SimpleJson.SerializeObject(item, this.CreateSerializerStrategy());

        /// <summary>
        ///     Deserialze a string to an object.
        /// </summary>
        /// <param name="text">The text to deserialize.</param>
        /// <typeparam name="T">The type of object to return.</typeparam>
        /// <returns>The deserialized object.</returns>
        public T Deserialize<T>(string text) => SimpleJson.DeserializeObject<T>(text, this.CreateSerializerStrategy());

        /// <summary>
        ///     Deserialze a string to an object.
        /// </summary>
        /// <param name="text">The text to deserialize.</param>
        /// <param name="type">The type of object to return.</param>
        /// <returns>The deserialized object.</returns>
        public object Deserialize(string text, Type type) => SimpleJson.DeserializeObject(text, type, this.CreateSerializerStrategy());

        /// <summary>
        ///     A method to map property names to output names/
        /// </summary>
        /// <param name="name">The name to map.</param>
        /// <returns>The value.</returns>
        public string MapName(Type parentType, string name) => this.CreateSerializerStrategy().MapName(parentType, name);

        public object DeserializePrimitiveValue(string value, Type valueType)
        {
            var strategy = this.CreateSerializerStrategy();
            return strategy.DeserializeObject(value, valueType);
        }

        public string SerializePrimitiveValue(object value) => SimpleJson.SerializeObject(value, this.CreateSerializerStrategy(), true);

        private CustomPocoJsonSerializerStrategy CreateSerializerStrategy()
            => new CustomPocoJsonSerializerStrategy(this.AllConverters(), true, false, null, null);
    }
}