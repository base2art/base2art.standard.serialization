﻿namespace Base2art.Serialization
{
    using System;

    /// <summary>
    ///     A contract that defines how serialization works.
    /// </summary>
    public interface IJsonSerializer
    {
        /// <summary>
        ///     Serialize an object to a string.
        /// </summary>
        /// <param name="item">The item to serialize.</param>
        /// <typeparam name="T">The type of object to serialize.</typeparam>
        /// <returns>The serialized value.</returns>
        string Serialize<T>(T item);

        /// <summary>
        ///     Deserialize a string to an object.
        /// </summary>
        /// <param name="text">The text to deserialize.</param>
        /// <typeparam name="T">The type of object to return.</typeparam>
        /// <returns>The deserialized object.</returns>
        T Deserialize<T>(string text);

        /// <summary>
        ///     Deserialize a string to an object.
        /// </summary>
        /// <param name="text">The text to deserialize.</param>
        /// <param name="type">The type of the object to return.</param>
        /// <returns>The deserialized object.</returns>
        object Deserialize(string text, Type type);

        /// <summary>
        ///     A method to map property names to output names/
        /// </summary>
        /// <param name="parentType">The type of object with which you are mapping.</param>
        /// <param name="name">The name to map.</param>
        /// <returns>The value.</returns>
        string MapName(Type parentType, string name);

        object DeserializePrimitiveValue(string key, Type objectType);

        string SerializePrimitiveValue(object value);
    }
}