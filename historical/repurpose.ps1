


if (Test-Path "Serialization")
{
    Remove-Item -Recurse -Force "Serialization"
}

if (Test-Path "Serialization.cs")
{
    Remove-Item -Recurse -Force "Serialization.cs"
}

./repurpose-for-dir.ps1 $args[0]
./repurpose-for-file.ps1 $args[0]
