

function ProcessFile($fileInfo, $output) {

    $items = Get-ChildItem -Path $fileInfo.FullName

    foreach ($item in $items) {

        if ($item.PSIsContainer) {
            if ($item.Name -eq "Properties"){
            } else {
                ProcessFile $item $output
            }
        } else
        {
            if ( $item.Name.EndsWith(".cs"))
            {
                $swallow = $output.Add($item)
            }
        }
    }
}

$root = Get-Item "src/Base2art.Standard.Serialization"
$data = New-Object System.Collections.ArrayList
ProcessFile $root $data


$cat = @("");
$space = @("", "")
foreach($item in $data){
    [string]$content = Get-Content -Path $item.FullName | out-String
    #$content = $content.Replace("#if NETFX_CORE`n#define SIMPLE_JSON_TYPEINFO`n#endif", "").Replace("#if NETFX_CORE`r`n#define SIMPLE_JSON_TYPEINFO`r`n#endif", "")
    
    $cat =  $cat + $space + $content
}


[string]$str = Out-String -InputObject $cat

$str = $str -replace "public abstract class", "internal abstract class"
$str = $str -replace "public class", "internal class"
$str = $str -replace "public sealed class", "internal sealed class"
$str = $str -replace "public static class", "internal static class"

$str = $str -replace "public abstract interface", "internal abstract interface"
$str = $str -replace "public interface", "internal interface"
$str = $str -replace "public sealed interface", "internal sealed interface"
$str = $str -replace "public static interface", "internal static interface"

$AppName = $args[0]
if ($AppName -eq $null) {
    $AppName = ""
}

if ($AppName -eq "") {
    $AppName = "Base2art.Test"
}

$str = $str -replace "namespace Base2art.Serialization", "namespace $($AppName)"

#namespace Base2art.Serialization

#$str = $str.Replace("public abstract class", "internal abstract class")
#           .Replace("public class", "internal class")

Set-Content -Path Serialization.cs -Value $str.Trim()


