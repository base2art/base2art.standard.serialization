#!/usr/bin/env bash


# sudo ln -s /usr/share/dotnet/dotnet /usr/bin/dotnet

#export PATH=/usr/share/dotnet:$PATH



echo "cleaning local packages..."
for d in src/* ; do
    rm $d/bin/Release/*.nupkg
done

echo "cleaning..."
rm -rf output-packages
TERM=xterm dotnet clean --configuration Release -v q

echo "building 1..."
TERM=xterm dotnet build --configuration Release -v q
echo "building 2..."
TERM=xterm dotnet build --configuration Release -v q

echo "testing..."
for d in test/* ; do
    TERM=xterm dotnet test --configuration Release -v q $d/*.csproj
done

echo "packing..."
TERM=xterm dotnet pack --include-symbols --include-source  -v q --configuration Release /p:PackageVersion=0.0.0.7


echo "testing..."
for d in test/* ; do
    TERM=xterm dotnet test --configuration Release -v q $d/*.csproj
done



mkdir -p output-packages

echo "pushing to local..."
for d in src/* ; do
    cp $d/bin/Release/*.nupkg /music/nuget-repository/
    cp $d/bin/Release/*.nupkg ./output-packages/
    rm ./output-packages/*.symbols.nupkg
done
