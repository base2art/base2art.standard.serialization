function cleanFile($content, $AppName)
{


    $str = $content


    $str = $str -replace "public abstract class", "internal abstract class"
    $str = $str -replace "public class", "internal class"
    $str = $str -replace "public sealed class", "internal sealed class"
    $str = $str -replace "public static class", "internal static class"

    $str = $str -replace "public abstract interface", "internal abstract interface"
    $str = $str -replace "public interface", "internal interface"
    $str = $str -replace "public sealed interface", "internal sealed interface"
    $str = $str -replace "public static interface", "internal static interface"

    $str = $str -replace "namespace Base2art.Serialization", "namespace $( $AppName )"

    return $str
}

function ProcessDir($fileInfo, $dest, $parts, $AppName)
{


    $items = Get-ChildItem -Path $fileInfo.FullName



    foreach ($item in $items)
    {


        if ($item.PSIsContainer)
        {
            if ($item.Name -eq "Properties")
            {
            }
            elseif ($item.Name -eq "bin")
            {
            }
            elseif ($item.Name -eq "obj")
            {
            }
            else
            {
                $newParts = $parts + @($item.Name)
                $dir = @($dest) + $parts + @( $item.Name )
                $pathOf = $dir -Join '/'
                
                $swallow = [System.IO.Directory]::CreateDirectory($pathOf)
                ProcessDir $item $dest $newParts $AppName
            }
        }
        else
        {
            if ( $item.Name.EndsWith(".cs"))
            {
                $newParts = @($dest) + $parts + @( $item.Name )

                $localDest = $newParts -Join '/'

                [string]$content = Get-Content -Path $item.FullName | out-String
                $content = cleanFile $content $AppName

                $cat = @("");
                $cat = $cat -Join [System.Environment]::NewLine
                $content = $cat + $content
                Set-Content -Path $localDest -Value $content
            }
        }
    }
}


$AppName = $args[0]
if ($AppName -eq $null)
{
    $AppName = ""
}

if ($AppName -eq "")
{
    $AppName = "Base2art.Test"
}


[System.Environment]::CurrentDirectory = pwd
Write-Host "Executing in $( [System.Environment]::CurrentDirectory )"

$root = Get-Item "src/Base2art.Standard.Serialization"


ProcessDir $root "Serialization" @() $AppName




