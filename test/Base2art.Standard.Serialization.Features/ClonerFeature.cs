﻿namespace Base2art.Serialization
{
    using System;
    using System.Collections.Generic;
    using System.Dynamic;
    using System.Linq.Expressions;
    using System.Net.Mail;
    using System.Reflection;
    using System.Text.RegularExpressions;
    using System.Xml;
    using System.Xml.Linq;
    using Fixtures;
    using FluentAssertions;
    using Moq;
    using Xunit;

    public class ClonerFeature
    {
        private class TempTypeNullableEnum
        {
            public Environment.SpecialFolder? Folder { get; set; }
        }

        private class TempTypeEnum
        {
            public Environment.SpecialFolder Folder { get; set; }
        }

        public T SerDer<T>(T input, string expected, IJsonSerializer ser = null)
        {
            ser = ser ?? new SimpleJsonSerializer();
            var str = ser.Serialize(input);
            str.Should().Be(expected);
            return ser.Deserialize<T>(str);
        }

        private void Field<T, TR>(T item, Expression<Func<T, TR>> p1, TR value)
        {
            var pe = (MemberExpression) p1.Body;
            var pi = (PropertyInfo) pe.Member;
            Console.WriteLine(pi.SetMethod.Invoke(item, new object[] {value}));
        }

        private void Compare<T>(T[] x, T[] y) where T : IComparable<T>
        {
            x.Length.Should().Be(y.Length);
            for (var i = 0; i < x.Length; i++)
            {
                x[i].Should().Be(y[i]);
            }
        }

        [Fact]
        public void ShouldClone_Implements_IClonable()
        {
            var person = new ClonablePerson {Name = "SjY"};

            var clonedPerson = person.CloneObject();

            clonedPerson.Name.Should().Be("SjY");
        }

        [Fact]
        public void ShouldClone_Implements_IClonable_BinarySerializer()
        {
            var person = new SerializablePerson {Name = "SjY"};

            var clonedPerson = person.CloneObject();

            clonedPerson.Name.Should().Be("SjY");
        }

        [Fact]
        public void ShouldClone_Implements_IClonable_DllNotFound()
        {
            var person = new ClonablePerson {Name = "SjY"};

            Func<ClonablePerson> mut = () => person.CloneObject(null);
            var item = mut();
            item.Should().NotBeNull();
        }

        [Fact]
        public void ShouldClone_Implements_NotIClonable_NullSerializer_Serialiable()
        {
            var person = new SerializablePerson {Name = "SjY"};
            //            Action mut = () => person.CloneObject(null);
            //            mut.ShouldThrow<System.Runtime.Serialization.SerializationException>();
            person.CloneObject(null).Should().NotBeNull();
        }

//        [Fact]
//        public void ShouldClone_Implements_NotIClonable_NullSerializer()
//        {
//            var person = new Person { Name = "SjY" };
//            Action mut = () => person.CloneObject(null);
//            mut.ShouldThrow<SerializationException>();
//        }

        [Fact]
        public void ShouldClone_Implements_NotIClonable_SerializerThrowingException()
        {
            var serializer = new Mock<IJsonSerializer>(MockBehavior.Strict);
            var person = new Person {Name = "SjY"};
            Action mut = () => person.CloneObject(serializer.Object);
            mut.Should().Throw<SerializationException>();
        }

        [Fact]
        public void ShouldClone_Null()
        {
            Cloner.CloneObject<string>(null).Should().BeNull();
        }

        [Fact]
        public void ShouldClone_ReusesInstance()
        {
            {
                var person = new Person {Name = "SjY"};
                var clonedPerson = person.CloneObject();
                clonedPerson.Name.Should().Be("SjY");
            }

            {
                var person = new Person {Name = "SjY"};
                var clonedPerson = person.CloneObject();
                clonedPerson.Name.Should().Be("SjY");
            }
        }

        // HACK
//        [Fact]
//        public void ShouldThrowExceptionSerializer_NullArgs()
//        {
//            var serializer = new BinaryItemSerializer<Person>();
//            new Action(() => serializer.Serialize(null)).ShouldThrow<ArgumentNullException>();
//            new Action(() => serializer.Deserialize(null)).ShouldThrow<ArgumentNullException>();
//            new Action(() => serializer.Deserialize(new byte[0])).ShouldThrow<ArgumentException>();
//        }

        [Fact]
        public void ShouldClone_Types()
        {
            "SjY".CloneObject().Should().Be("SjY");
            1.CloneObject().Should().Be(1);
            1L.CloneObject().Should().Be(1L);
        }

        [Fact]
        public void ShouldClone_WithSerialization()
        {
            var person = new Person {Name = "SjY"};

            var clonedPerson = person.CloneObject();

            clonedPerson.Name.Should().Be("SjY");
        }

        [Fact]
        public void ShouldCloneAllTypes()
        {
            var g = Guid.NewGuid();
            var dayTime = DateTime.UtcNow;
            var item = new AllFieldTypesInterfaceV1();

            var xml = new XmlDocument();
            xml.LoadXml("<test1>" + g.ToString("N") + "</test1>");

            this.Field(item, x => x.Boolean_value, true);
            this.Field(item, x => x.Decimal_value, 10.0m);
            this.Field(item, x => x.Double_value, double.MaxValue / 2);
            this.Field(item, x => x.Float_value, float.MaxValue / 2);
            this.Field(item, x => x.int_value, int.MaxValue / 2);
            this.Field(item, x => x.long_value, int.MaxValue);
            this.Field<AllFieldTypesInterfaceV1, short>(item, x => x.short_value, short.MaxValue / 2);
            this.Field(item, x => x.string_value, g.ToString("N"));
            this.Field(item, x => x.binary_value, g.ToByteArray());
            this.Field(item, x => x.date_value, dayTime.Date);
            this.Field(item, x => x.dateTimeOffset_value, dayTime);
            this.Field(item, x => x.datetime_value, dayTime);
            this.Field(item, x => x.interval_value, dayTime.TimeOfDay);
            this.Field(item, x => x.guid_value, g);
            this.Field(item, x => x.version, new Version(1, 0, 0, 0));
            this.Field(item, x => x.xml1_value, xml);
            this.Field(item, x => x.xml2_value, XElement.Parse("<test2>" + g.ToString("N") + "</test2>"));
            this.Field(item, x => x.json_value, new Dictionary<string, object>
                                                {
                                                    {
                                                        "a",
                                                        1
                                                    }
                                                });
            this.Field(item, x => x.Boolean_nullable_value, true);
            this.Field(item, x => x.Decimal_nullable_value, 10.0m);
            this.Field(item, x => x.Double_nullable_value, double.MaxValue / 2);
            this.Field(item, x => x.Float_nullable_value, float.MaxValue / 2);
            this.Field(item, x => x.int_nullable_value, int.MaxValue / 2);
            this.Field(item, x => x.long_nullable_value, int.MaxValue);
            this.Field(item, x => x.short_nullable_value, (short) (short.MaxValue / 2));
            this.Field(item, x => x.date_nullable_value, dayTime.Date);
            this.Field(item, x => x.datetime_nullable_value, dayTime);
            this.Field(item, x => x.dateTimeOffset_nullable_value, dayTime);
            this.Field(item, x => x.interval_nullable_value, dayTime.TimeOfDay);
            this.Field(item, x => x.guid_nullable_value, g);
            var ser = new SimpleJsonSerializer();
            var str = ser.Serialize(item);

            str.Should().Contain(dayTime.TimeOfDay.ToString("c"));
            str.Should().Contain("1.0.0.0");
//            str.Should().Contain("dfsdfsdf");
            Console.WriteLine(str);

            var data = ser.Deserialize<AllFieldTypesInterfaceV1>(str);
//
            this.Compare(data.binary_value, g.ToByteArray());
            data.Boolean_nullable_value.Should().BeTrue();
            data.Boolean_value.Should().BeTrue();
            data.date_nullable_value.Should().BeCloseTo(DateTime.UtcNow.Date);
            data.date_value.Should().BeCloseTo(DateTime.UtcNow.Date);
            data.datetime_nullable_value.Should().BeCloseTo(DateTime.UtcNow, 2499);
            data.datetime_value.Should().BeCloseTo(DateTime.UtcNow, 2499);
            data.Decimal_nullable_value.Should().Be(10.0m);
            data.Decimal_value.Should().Be(10m);
            data.Double_nullable_value.Should().Be(double.MaxValue / 2);
            data.Double_value.Should().Be(double.MaxValue / 2);
            data.Float_nullable_value.Should().BeApproximately(float.MaxValue / 2, 3.04236144E+35F);
            data.Float_value.Should().BeApproximately(float.MaxValue / 2, 3.04236144E+35F);
            data.guid_nullable_value.Should().Be(g);
            data.guid_value.Should().Be(g);
            data.int_nullable_value.Should().Be(int.MaxValue / 2);
            data.int_value.Should().Be(int.MaxValue / 2);
            data.long_nullable_value.Should().Be(int.MaxValue);
            data.long_value.Should().Be(int.MaxValue);
            data.short_nullable_value.Should().Be(short.MaxValue / 2);
            data.short_value.Should().Be(short.MaxValue / 2);
            data.interval_nullable_value.Should().BeCloseTo(DateTime.UtcNow.TimeOfDay, 4099);
            data.interval_value.Should().BeCloseTo(DateTime.UtcNow.TimeOfDay, 4099);
//                        data.object_value.Should().Be(null);
            data.json_value.Count.Should().Be(1);
            data.string_value.Should().Be(g.ToString("N"));
            data.version.Should().Be(new Version("1.0.0.0"));
            data.xml1_value.DocumentElement.Name.Should().Be("test1");
            data.xml2_value.Name.LocalName.Should().Be("test2");
            //            data.interval_value.Should().Be(int.MaxValue);
        }

        [Fact]
        public void ShouldCloneAnonymousNestedType()
        {
            Environment.SpecialFolder? folder = Environment.SpecialFolder.ProgramFiles;
            var temp = new {Other = new {Folder = folder}};
            var result = this.SerDer(temp, "{\"Other\":{\"Folder\":\"ProgramFiles\"}}");
            result.Other.Folder.Should().Be(folder);

            folder = null;
            var temp1 = new {Other = new {Folder = folder}};
            var result1 = this.SerDer(temp1, "{\"Other\":{\"Folder\":null}}");
            result1.Other.Folder.Should().Be(folder);
        }

        [Fact]
        public void ShouldCloneAnonymousType()
        {
            Environment.SpecialFolder? folder = Environment.SpecialFolder.ProgramFiles;
            var temp = new {Folder = folder};
            var result = this.SerDer(temp, "{\"Folder\":\"ProgramFiles\"}");
            result.Folder.Should().Be(folder);

            folder = null;
            var temp1 = new {Folder = folder};
            var result1 = this.SerDer(temp1, "{\"Folder\":null}");
            result1.Folder.Should().Be(folder);
        }

        [Fact]
        public void ShouldCloneArray()
        {
            var temp = new[] {"a", "b"};

            var result = this.SerDer(temp, "[\"a\",\"b\"]");
            result.Should().HaveCount(2);
            result[0].Should().Be("a");
            result[1].Should().Be("b");
        }

        [Fact]
        public void ShouldCloneArrayObject()
        {
            var temp = new[]
                       {
                           new Person {Name = "Scott"},
                           new Person {Name = "Matt"}
                       };

            var result = this.SerDer(temp,
                                     "[{\"HighestGradeLevel\":\"MiddleSchool\",\"Name\":\"Scott\",\"Aliases\":null,\"Birthday\":\"0001-01-01T00:00:00\"},{\"HighestGradeLevel\":\"MiddleSchool\",\"Name\":\"Matt\",\"Aliases\":null,\"Birthday\":\"0001-01-01T00:00:00\"}]");
            result.Should().HaveCount(2);
            result[0].Name.Should().Be("Scott");
            result[1].Name.Should().Be("Matt");
        }

        [Fact]
        public void ShouldCloneBinary()
        {
            var ser = new SimpleJsonSerializer();
            var str = ser.Serialize(new {binary = new byte[] {1, 2, 3, 4}});
            str.Should().Be("{\"binary\":\"AQIDBA==\"}");
        }

        [Fact]
        public void ShouldCloneDictionary()
        {
            var ser = new SimpleJsonSerializer();
            var str = ser.Serialize(new Dictionary<string, string> {{"Message", "test"}});
            str.Should().Be("{\"Message\":\"test\"}");
            var result = ser.Deserialize<Dictionary<string, string>>(str);
            result["Message"].Should().Be("test");

            var str1 = ser.Serialize(new Dictionary<string, object> {{"Message", "test"}, {"Count", 2}});
            str1.Should().Be("{\"Message\":\"test\",\"Count\":2}");
            var result1 = ser.Deserialize<Dictionary<string, object>>(str1);
            result1["Message"].Should().Be("test");
            result1["Count"].Should().Be(2L);
        }

        [Fact]
        public void ShouldCloneDictionaryCamelCase()
        {
            var ser = new CamelCasingSimpleJsonSerializer();
            var str = ser.Serialize(new Dictionary<string, string> {{"Message", "test"}});
            str.Should().Be("{\"Message\":\"test\"}");
            var result = ser.Deserialize<Dictionary<string, string>>(str);
            result["Message"].Should().Be("test");

            var str1 = ser.Serialize(new Dictionary<string, object> {{"Message", "test"}, {"Count", 2}});
            str1.Should().Be("{\"Message\":\"test\",\"Count\":2}");
            var result1 = ser.Deserialize<Dictionary<string, object>>(str1);
            result1["Message"].Should().Be("test");
            result1["Count"].Should().Be(2L);
        }

        [Fact]
        public void ShouldCloneEnum()
        {
            var folder = Environment.SpecialFolder.ProgramFiles;
            var temp = new TempTypeEnum {Folder = folder};
            var result = this.SerDer(temp, "{\"Folder\":\"ProgramFiles\"}");
            result.Folder.Should().Be(folder);

            folder = default(Environment.SpecialFolder);
            var temp1 = new TempTypeEnum {Folder = folder};
            var result1 = this.SerDer(temp1, "{\"Folder\":\"Desktop\"}");
            result1.Folder.Should().Be(folder);
        }

        [Fact]
        public void ShouldCloneExpandoObject()
        {
            var input = new {Message = new ExpandoObject()};

            dynamic d = input.Message;
            d.Key = "abc";
            d.Value = 1;

            var rez = this.SerDer(input, "{\"Message\":{\"Key\":\"abc\",\"Value\":1}}");

//            ((dynamic)rez.Message).Key.Should().Be("abc");
//            ((dynamic)rez.Message).Value.Should().Be(1);
        }

        [Fact]
        public void ShouldCloneKeyValuePair()
        {
            var ser = new SimpleJsonSerializer();
            var input = new {Message = new KeyValuePair<string, int>("abc", 1)};

            var rez = this.SerDer(input, "{\"Message\":{\"Key\":\"abc\",\"Value\":1}}");

            rez.Message.Key.Should().Be("abc");
            rez.Message.Value.Should().Be(1);
        }

        [Fact]
        public void ShouldCloneKeyValuePairCamelCase()
        {
            var ser = new CamelCasingSimpleJsonSerializer();
            var input = new {Message = new KeyValuePair<string, int>("abc", 1)};

            var rez = this.SerDer(input, "{\"message\":{\"key\":\"abc\",\"value\":1}}", ser);

            rez.Message.Key.Should().Be("abc");
            rez.Message.Value.Should().Be(1);
        }

        [Fact]
        public void ShouldCloneList()
        {
            var temp = new List<string>();
            temp.Add("a");
            temp.Add("b");

            var result = this.SerDer(temp, "[\"a\",\"b\"]");
            result.Should().HaveCount(2);
            result[0].Should().Be("a");
            result[1].Should().Be("b");
        }

        [Fact]
        public void ShouldCloneListObject()
        {
            var temp = new List<Person>();
            temp.Add(new Person {Name = "Scott"});
            temp.Add(new Person {Name = "Matt"});

            var result = this.SerDer(temp,
                                     "[{\"HighestGradeLevel\":\"MiddleSchool\",\"Name\":\"Scott\",\"Aliases\":null,\"Birthday\":\"0001-01-01T00:00:00\"},{\"HighestGradeLevel\":\"MiddleSchool\",\"Name\":\"Matt\",\"Aliases\":null,\"Birthday\":\"0001-01-01T00:00:00\"}]");
            result.Should().HaveCount(2);
            result[0].Name.Should().Be("Scott");
            result[1].Name.Should().Be("Matt");
        }

        [Fact]
        public void ShouldCloneMailAddress()
        {
            var ser = new SimpleJsonSerializer();
            var input = new {Message = new MailAddress("scott@example.com", "Scott Test")};

            var rez = this.SerDer(input, "{\"Message\":\"\\\"Scott Test\\\" <scott@example.com>\"}");

            rez.Message.Address.Should().Be("scott@example.com");
            rez.Message.DisplayName.Should().Be("Scott Test");
        }

        [Fact]
        public void ShouldCloneNullabeEnum()
        {
            Environment.SpecialFolder? folder = Environment.SpecialFolder.ProgramFiles;
            var temp = new TempTypeNullableEnum {Folder = folder};
            var result = this.SerDer(temp, "{\"Folder\":\"ProgramFiles\"}");
            result.Folder.Should().Be(folder);

            folder = null;
            var temp1 = new TempTypeNullableEnum {Folder = folder};
            var result1 = this.SerDer(temp1, "{\"Folder\":null}");
            result1.Folder.Should().Be(folder);
        }

        [Fact]
        public void ShouldCloneRegex()
        {
            var ser = new SimpleJsonSerializer();
            var input = new {Message = new Regex("\\d\\d", RegexOptions.IgnoreCase | RegexOptions.CultureInvariant)};

            var rez = this.SerDer(input, "{\"Message\":{\"Pattern\":\"\\\\d\\\\d\",\"Options\":\"IgnoreCase, CultureInvariant\"}}");

            rez.Message.ToString().Should().Be("\\d\\d");
            rez.Message.Options.Should().Be(RegexOptions.IgnoreCase | RegexOptions.CultureInvariant);
        }

        [Fact]
        public void ShouldSerialize_WithSerialization()
        {
            var person = new Person {Name = "SjY"};

            var result = new SimpleJsonSerializer().Serialize(person);

            result.Should().Be("{\"HighestGradeLevel\":\"MiddleSchool\",\"Name\":\"SjY\",\"Aliases\":null,\"Birthday\":\"0001-01-01T00:00:00\"}");
        }

        // HACK
//        [Fact]
//        public void Should_Serialize_Deserialize_ManualCall()
//        {
//            var person = new SerializablePerson { Name = "SjY" };
//            
//            var serializer = new BinaryItemSerializer<SerializablePerson>();
//            var result = serializer.Serialize(person);
//            
//            var person1 = serializer.Deserialize(result);
//            
//            var ser = new SimpleSerializer();
//            
//            ser.Serialize(person)
//                .Should().Be(ser.Serialize(person1));
////            result.Should().Be("{\r\n  \"Name\": \"SjY\",\r\n  \"Aliases\": null,\r\n  \"HighestGradeLevel\": \"MiddleSchool\",\r\n  \"Birthday\": \"0001-01-01T00:00:00\"\r\n}");
//        }

        [Fact]
        public void ShouldSerialize_WithSerialization_UsingDefaultCache()
        {
            Action call = () =>
            {
                var person = new Person {Name = "SjY"};

                var result = new SimpleJsonSerializer().Serialize(person);

                result.Should().Be("{\"HighestGradeLevel\":\"MiddleSchool\",\"Name\":\"SjY\",\"Aliases\":null,\"Birthday\":\"0001-01-01T00:00:00\"}");
            };

            call();
            call();
        }
    }
}