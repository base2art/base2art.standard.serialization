﻿namespace Base2art.Dapper
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.Remoting.Messaging;
    using System.Runtime.Remoting.Proxies;
    using FluentAssertions.Common;

    public class TypedDictionaryProxy<T> : RealProxy
    {
        private readonly IReadOnlyDictionary<string, object> data;

        public TypedDictionaryProxy(IReadOnlyDictionary<string, object> data)
            : base(typeof(T)) => this.data = data;

        public T Data => (T) this.GetTransparentProxy();

        public override IMessage Invoke(IMessage msg)
        {
            var methodCall = msg as IMethodCallMessage;

            return methodCall != null
                       ? this.HandleMethodCall(methodCall)
                       : null;
        }

        private IMessage HandleMethodCall(IMethodCallMessage methodCall)
        {
            var method = (MethodInfo) methodCall.MethodBase;
            var isGetOrSetAccessor = method.DeclaringType.GetPublicProperties()
                                           .Any(prop => prop.GetGetMethod() == method || prop.GetSetMethod() == method);

            var name = method.Name;
            if (isGetOrSetAccessor)
            {
                // get_
                // or
                // set_
                name = method.Name.Substring(4);
            }

            try
            {
                var context = methodCall.LogicalCallContext;
                if (this.data.ContainsKey(name))
                {
                    var result = this.data[name];

                    if (result == null)
                    {
                        return new ReturnMessage(null, null, 0, context, methodCall);
                    }

                    var foo = TypeDescriptor.GetConverter(result.GetType());
                    if (foo.CanConvertTo(method.ReturnType))
                    {
                        result = foo.ConvertTo(result, method.ReturnType);
                    }

                    return new ReturnMessage(result, null, 0, context, methodCall);
                }

                if (method.ReturnType.GetTypeInfo().IsClass)
                {
                    return new ReturnMessage(null, null, 0, context, methodCall);
                }

                return new ReturnMessage(Activator.CreateInstance(method.ReturnType), null, 0, context, methodCall);
            }
            catch (TargetInvocationException invocationException)
            {
                return new ReturnMessage(invocationException.InnerException, methodCall);
            }
        }
    }
}