﻿namespace Base2art.Serialization
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Text;
    using System.Xml;
    using Dapper;
    using Fixtures;
    using FluentAssertions;
    using Xunit;

    public class RealWorldFeature
    {
        private void Field<T, TR>(T item, Expression<Func<T, TR>> p1, TR value)
        {
            var pe = (MemberExpression) p1.Body;
            var pi = (PropertyInfo) pe.Member;
            Console.WriteLine(pi.SetMethod.Invoke(item, new object[] {value}));
        }

        [Fact]
        public void ShouldClone_ByteArray()
        {
            var encoding = Encoding.UTF8;
            var doc = new SerializableDocument
                      {
                          Name = "SjY",
                          Content = encoding.GetBytes("SjY")
                      };

            var ser = new SimpleJsonSerializer();
            var str = ser.Serialize(doc);

            str.Should().Be(@"{""Name"":""SjY"",""Content"":""U2pZ""}");

            encoding.GetString(ser.Deserialize<SerializableDocument>(str).Content)
                    .Should().Be("SjY");
        }

        [Fact]
        public void ShouldSkipCertainItems()
        {
            var encoding = Encoding.UTF8;
            var doc = new SerializableDocument
                      {
                          Name = "SjY",
                          Content = encoding.GetBytes("SjY")
                      };

            var ser = new ConfigurableSimpleJsonSerializer(
                                                           true,
                                                           true,
                                                           null,
                                                           (t, p) => t == typeof(SerializableDocument) && p == nameof(SerializableDocument.Content),
                                                           (t, p) => null);
            var str = ser.Serialize(doc);

            str.Should().Be(
                            @"{
  ""name"": ""SjY""
}");

            ser.Deserialize<SerializableDocument>(str).Content.Should().BeNull();
        }

        [Fact]
        public void ShouldSerializeRealProxy()
        {
            var dict = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
            var data = new TypedDictionaryProxy<IPerson>(dict).Data;
            dict[nameof(IPerson.Name)] = "SjY";

            var ser = new CamelCasingIndentedSimpleJsonSerializer();
            var str = ser.Serialize(data);

            str.Should().Be(
                            @"{
  ""name"": ""SjY"",
  ""birthday"": ""0001-01-01T00:00:00"",
  ""aliases"": null
}");

//            ser.Deserialize<SerializableDocument>(str).Content.Should().BeNull();
        }

        [Fact]
        public void ShouldFormatIndented()
        {
            var encoding = Encoding.UTF8;
            var doc = new SerializableDocument
                      {
                          Name = "SjY",
                          Content = encoding.GetBytes("SjY")
                      };

            var ser = new CamelCasingIndentedSimpleJsonSerializer();
            var str = ser.Serialize(doc);

            str.Should().Be(
                            @"{
  ""name"": ""SjY"",
  ""content"": ""U2pZ""
}");

            encoding.GetString(ser.Deserialize<SerializableDocument>(str).Content)
                    .Should().Be("SjY");
        }

        [Fact]
        public void ShouldFormatIndentedNested()
        {
            var encoding = Encoding.UTF8;

            var items = new Dictionary<string, object>
                        {
                            {"a", "b"},
                            {"c", 3},
                            {"d", new[] {"e", "f", "g"}},
                            {"h", new[] {1, 2, 3, 4}},
                            {
                                "i", new[]
                                     {
                                         new SerializableDocument
                                         {
                                             Name = "SjY",
                                             Content = encoding.GetBytes("SjY")
                                         },
                                         new SerializableDocument
                                         {
                                             Name = "MjY",
                                             Content = encoding.GetBytes("MjY")
                                         }
                                     }
                            },
                        };
            var ser = new CamelCasingIndentedSimpleJsonSerializer();
            var str = ser.Serialize(items);

            str.Should().Be(
                            @"{
  ""a"": ""b"",
  ""c"": 3,
  ""d"": [
    ""e"",
    ""f"",
    ""g""
  ],
  ""h"": [
    1,
    2,
    3,
    4
  ],
  ""i"": [
    {
      ""name"": ""SjY"",
      ""content"": ""U2pZ""
    },
    {
      ""name"": ""MjY"",
      ""content"": ""TWpZ""
    }
  ]
}");

//            encoding.GetString(ser.Deserialize<SerializableDocument>(str).Content)
//                    .Should().Be("SjY");
        }

        [Fact]
        public void ShouldCloneAllTypes()
        {
            var g = Guid.NewGuid();
            var dayTime = DateTime.UtcNow;
            var item = new RealWorldXmlFixture();

            var xml = new XmlDocument();
            xml.LoadXml("<test1>" + g.ToString("N") + "</test1>");

            this.Field(item, x => x.xml1_value, xml);
            var ser = new SimpleJsonSerializer();
            var str = ser.Serialize(item);

            Console.WriteLine(str);

            var data = ser.Deserialize<RealWorldXmlFixture>(str);
            data.xml1_value.DocumentElement.Name.Should().Be("test1");
//            data.xml2_value.Name.LocalName.Should().Be("test2");
        }

        [Fact]
        public void ShouldSerializeListOfItems()
        {
            var encoding = Encoding.UTF8;
            var doc = new SerializableDocument
                      {
                          Name = "SjY",
                          Content = encoding.GetBytes("SjY")
                      };

            var ser = new SimpleJsonSerializer();
            var str = ser.Serialize(new List<SerializableDocument> {doc});

            str.Should().Be(@"[{""Name"":""SjY"",""Content"":""U2pZ""}]");

            ser.Deserialize<List<SerializableDocument>>(str)
               .Should().HaveCount(1)
               .And.Subject.First().Name.Should().Be("SjY");
        }

        [Fact]
        public void ShouldDeserializeLookup()
        {
            var content1 = "{ 'data1': 1, 'data2':  2, 'data3':  4 } ";

            var ser = new SimpleJsonSerializer();
            var data1 = ser.Deserialize<Dictionary<string, int>>(content1);
            data1["data3"].Should().Be(4);
        }

        [Fact]
        public void ShouldDeserializeListNotLast()
        {
            var content1 = @"
[
  {
    ""id"": ""base2art:npm-command:0.0.0.1"",
    ""data"":  { 
      ""command"": ""run"", 
      ""targetPath"": ""src/Site/assets/package.json"",
      ""parameters"": [""tsc""]
    }
  }
]
";
            var content2 = @"
[
  {
    ""id"": ""base2art:npm-command:0.0.0.1"",
    ""data"":  { 
      ""command"": ""run"",
      ""parameters"": [""tsc""], 
      ""targetPath"": ""src/Site/assets/package.json""
    }
  }
]
";

            var ser = new CamelCasingIndentedSimpleJsonSerializer();
            var data1 = ser.Deserialize<List<ProcedureData>>(content1);
            var data2 = ser.Deserialize<List<ProcedureData>>(content2);

            Clean(ser.Serialize(data1[0].Data)).Should().Be(Clean(@"{
  ""command"": ""run"",
  ""targetPath"": ""src/Site/assets/package.json"",
  ""parameters"": [
    ""tsc""
  ]
}"));

            Clean(ser.Serialize(data2[0].Data)).Should().Be(Clean(@"{
  ""command"": ""run"",
  ""parameters"": [
    ""tsc""
  ],
  ""targetPath"": ""src/Site/assets/package.json""
}"));

            var o1 = data1[0].Data["parameters"];
            var o2 = data2[0].Data["parameters"];
            o1.As<IEnumerable<object>>().Should().HaveCount(1).And.Subject.First().Should().Be("tsc");
            o2.As<IEnumerable<object>>().Should().HaveCount(1).And.Subject.First().Should().Be("tsc");
        }

        [Fact]
        public void ShouldDeserializeRuntimes_Json()
        {
            string GetContent()
            {
                return ReadResourceAsString(
                                            Assembly.GetExecutingAssembly(),
                                            "Base2art.Serialization.runtime.json");
            }

            var content = GetContent();
            var serializer = new CamelCasingIndentedSimpleJsonSerializer();
            {
                var result = serializer.Deserialize<RuntimesFileArray>(content);

                var resultValue = result.Runtimes["alpine"]["#import"];
                resultValue.Should().HaveCount(1);
                resultValue[0].Should().Be("linux-musl");
            }
            {
                var result = serializer.Deserialize<RuntimesFileList>(content);

                var resultValue = result.Runtimes["alpine"]["#import"];
                resultValue.Should().HaveCount(1);
                resultValue[0].Should().Be("linux-musl");
            }
        }

        private string Clean(string x)
        {
            x = x.Trim().Replace("\r\n", "\n\n");

            while (x.Contains("\n\n"))
            {
                x = x.Replace("\n\n", "\n");
            }

            return x;
        }

        public static string ReadResourceAsString(Assembly asm, string resourceName)
        {
            var stream = asm.GetManifestResourceStream(resourceName);

            using (var sr = new System.IO.StreamReader(stream))
            {
                return sr.ReadToEnd();
            }
        }

        private class RuntimesFileArray
        {
            public Dictionary<string, Dictionary<string, string[]>> Runtimes { get; set; }
        }

        private class RuntimesFileList
        {
            public Dictionary<string, Dictionary<string, List<string>>> Runtimes { get; set; }
        }

        private class ProcedureData
        {
            public string Id { get; set; }
            public string Org { get; set; }
            public string Name { get; set; }
            public string Version { get; set; }

            public Dictionary<string, object> Data { get; set; }
        }
    }
}