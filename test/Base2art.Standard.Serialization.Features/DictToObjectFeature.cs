﻿namespace Base2art.Serialization
{
    using System;
    using System.Collections.Generic;
    using Fixtures;
    using FluentAssertions;
    using Xunit;

    public class SimpleDictToObjectFeature
    {
        [Theory]
        [InlineData("{}", null)]
        [InlineData("{\"name\":\"Test\"}", "Test")]
        [InlineData("{name:\"Test\"}", "Test")]
        [InlineData("{name\t:\"Test\"}", "Test")]
        [InlineData("{'name':\"Test\"}", "Test")]
        public void ShouldLoad(string content, string nameExpected)
        {
            var json = new CamelCasingSimpleJsonSerializer();
            var person = json.Deserialize<Person>(content);
            person.Name.Should().Be(nameExpected);
        }

        [Theory]
        [InlineData("{name 1:\"Test\"}")]
        [InlineData("{name':'Test'}")]
        [InlineData("{name\\':'Test'}")]
        public void ShouldLoad1(string content)
        {
            Action act = () =>
            {
                var json = new CamelCasingSimpleJsonSerializer();
                var person = json.Deserialize<Person>(content);
            };

            act.Should().Throw<SerializationException>();
        }

        [Theory]
        [InlineData("Name", "test")]
        [InlineData("name", "test")]
        [InlineData("other", null)]
        public void ShouldConvertSimple(string propertyName, string expected)
        {
            var json = new CamelCasingSimpleJsonSerializer();

            var personValue = new Person();
            personValue.Name = "test";

//            var personText = json.Serialize(personValue);
            var personText = "{\"" + propertyName + "\":\"test\"}";
            var person = json.Deserialize<Person>(personText);

            person.Name.Should().Be(expected);

            var dynamicPerson = json.Deserialize<dynamic>(personText);
            ((string) dynamicPerson.Name).Should().Be(expected);
        }

        [Theory]
        [InlineData("Name", "test")]
        [InlineData("name", "test")]
        [InlineData("other", null)]
        public void ShouldConvertDict(string propertyName, string expected)
        {
            var json = new CamelCasingSimpleJsonSerializer();

            var dictionary = new Dictionary<string, object>();
            dictionary[propertyName] = "test";

            var obj = json.Serialize(dictionary);
            var person = json.Deserialize<Person>(obj);

            person.Name.Should().Be(expected);

            var dynamicPerson = json.Deserialize<dynamic>(obj);
            ((string) dynamicPerson.Name).Should().Be(expected);
        }

        [Fact]
        public void ShouldConvertDictEnumPerson()
        {
            var json = new CamelCasingSimpleJsonSerializer();

            var key = Environment.SpecialFolder.Cookies;
            var dictionary = new Dictionary<Environment.SpecialFolder, Person>();
            dictionary[key] = new Person
                              {
                                  Name = "Leat Hakkor"
                              };

            var obj = json.Serialize(dictionary);
            obj.Should()
               .Be("{\"Cookies\":"
                   + "{\"highestGradeLevel\":\"MiddleSchool\",\"name\":\"Leat Hakkor\",\"aliases\":null,\"birthday\":\"0001-01-01T00:00:00\"}}");
            obj.Should().StartWith("{\"Cookies\":{\"");
            var person = json.Deserialize<Dictionary<Environment.SpecialFolder, Person>>(obj);

            person[key].Name.Should().Be("Leat Hakkor");
        }

        [Fact]
        public void ShouldConvertDictGuidPerson()
        {
            var json = new CamelCasingSimpleJsonSerializer();

            var key = new Guid("f9ed3299-ee2f-4b5c-8277-d1223a986bb6");
            var dictionary = new Dictionary<Guid, Person>();
            dictionary[key] = new Person
                              {
                                  Name = "Leat Hakkor"
                              };

            var obj = json.Serialize(dictionary);
            obj.Should()
               .Be("{\"f9ed3299-ee2f-4b5c-8277-d1223a986bb6\":"
                   + "{\"highestGradeLevel\":\"MiddleSchool\",\"name\":\"Leat Hakkor\",\"aliases\":null,\"birthday\":\"0001-01-01T00:00:00\"}}");
            obj.Should().StartWith("{\"f9ed3299-ee2f-4b5c-8277-d1223a986bb6\":{\"");
            var person = json.Deserialize<Dictionary<Guid, Person>>(obj);

            person[key].Name.Should().Be("Leat Hakkor");
        }

        [Fact]
        public void ShouldConvertDictInt32Person()
        {
            var json = new CamelCasingSimpleJsonSerializer();

            var key = 42;
            var dictionary = new Dictionary<int, Person>();
            dictionary[key] = new Person
                              {
                                  Name = "Leat Hakkor"
                              };

            var obj = json.Serialize(dictionary);
            obj.Should()
               .Be("{\"42\":"
                   + "{\"highestGradeLevel\":\"MiddleSchool\",\"name\":\"Leat Hakkor\",\"aliases\":null,\"birthday\":\"0001-01-01T00:00:00\"}}");
            obj.Should().StartWith("{\"42\":{\"");
            var person = json.Deserialize<Dictionary<int, Person>>(obj);

            person[key].Name.Should().Be("Leat Hakkor");
        }

        [Fact]
        public void ShouldConvertDictStringPerson()
        {
            var json = new CamelCasingSimpleJsonSerializer();

            var dictionary = new Dictionary<string, Person>();
            dictionary["LH"] = new Person
                               {
                                   Name = "Leat Hakkor"
                               };

            var obj = json.Serialize(dictionary);
            obj.Should().StartWith("{\"LH\":{\"");
            obj.Should()
               .Be("{\"LH\":{\"highestGradeLevel\":\"MiddleSchool\",\"name\":\"Leat Hakkor\",\"aliases\":null,\"birthday\":\"0001-01-01T00:00:00\"}}");
            var person = json.Deserialize<Dictionary<string, Person>>(obj);

            person["LH"].Name.Should().Be("Leat Hakkor");
        }

        [Fact]
        public void ShouldConvertDictStringPersonArray()
        {
            var json = new CamelCasingSimpleJsonSerializer();

            var dictionary = new Dictionary<string, Person[]>();
            dictionary["LH"] = new Person[1]
                               {
                                   new Person
                                   {
                                       Name = "Leat Hakkor"
                                   }
                               };

            var obj = json.Serialize(dictionary);
            obj.Should().StartWith("{\"LH\":[{\"");
            obj.Should()
               .Be("{\"LH\":[{\"highestGradeLevel\":\"MiddleSchool\",\"name\":\"Leat Hakkor\",\"aliases\":null,\"birthday\":\"0001-01-01T00:00:00\"}]}");
            var person = json.Deserialize<Dictionary<string, Person[]>>(obj);

            person["LH"][0].Name.Should().Be("Leat Hakkor");
        }

        [Fact]
        public void ShouldConvertDictWithList()
        {
            var items = new Dictionary<string, object>();
            items["name"] = "SjY";
            items["tags"] = new List<string> {"male", "tall"};

            var json = new CamelCasingSimpleJsonSerializer();
            var result = json.Serialize(items);

            result.Should().Be("{\"name\":\"SjY\",\"tags\":[\"male\",\"tall\"]}");

            var result2 = json.Deserialize<Dictionary<string, object>>(result);

            result2["tags"].Should().NotBeNull();
        }

        [Fact]
        public void ShouldConvertSortedDictStringPerson()
        {
            var json = new CamelCasingSimpleJsonSerializer();

            var dictionary = new SortedDictionary<string, Person>();
            dictionary["LH"] = new Person
                               {
                                   Name = "Leat Hakkor"
                               };

            var obj = json.Serialize(dictionary);

            obj.Should()
               .Be("{\"LH\":{\"highestGradeLevel\":\"MiddleSchool\",\"name\":\"Leat Hakkor\",\"aliases\":null,\"birthday\":\"0001-01-01T00:00:00\"}}");

            obj.Should().StartWith("{\"LH\":{\"");
            var person = json.Deserialize<Dictionary<string, Person>>(obj);

            person["LH"].Name.Should().Be("Leat Hakkor");
        }
    }
}