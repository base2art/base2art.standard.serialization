﻿namespace Base2art.Serialization.Fixtures
{
    using System.Xml;

    public class RealWorldXmlFixture
    {
        public XmlDocument xml1_value { get; set; }
    }
}