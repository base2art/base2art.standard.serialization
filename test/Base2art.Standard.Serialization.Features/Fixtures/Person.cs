﻿namespace Base2art.Serialization.Fixtures
{
    using System;

    public class Person : IPerson
    {
        public SchoolLevel HighestGradeLevel { get; set; }

        public string Name { get; set; }

        public string[] Aliases { get; set; }

        public DateTime Birthday { get; set; }
    }
}