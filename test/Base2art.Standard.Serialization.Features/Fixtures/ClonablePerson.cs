﻿namespace Base2art.Serialization.Fixtures
{
    using System;

    public class ClonablePerson : IPerson, ICloneable
    {
        object ICloneable.Clone() => this.Clone();

        public string Name { get; set; }

        public string[] Aliases { get; set; }

        public DateTime Birthday { get; set; }

        public ClonablePerson Clone() => (ClonablePerson) this.MemberwiseClone();
    }
}