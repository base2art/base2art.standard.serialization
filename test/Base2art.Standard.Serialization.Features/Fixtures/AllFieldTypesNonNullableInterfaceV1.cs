﻿namespace Base2art.Serialization.Fixtures
{
    using System;

    public class AllFieldTypesNonNullableInterfaceV1
    {
        public bool Boolean_value { get; set; }
        public decimal Decimal_value { get; set; }
        public double Double_value { get; set; }
        public float Float_value { get; set; }
        public int int_value { get; set; }
        public long long_value { get; set; }
        public short short_value { get; set; }
        public string string_value { get; set; }
        public DateTime date_value { get; set; }
        public DateTimeOffset dateTimeOffset_value { get; set; }
        public DateTime datetime_value { get; set; }
        public TimeSpan interval_value { get; set; }
        public Guid guid_value { get; set; }
    }
}