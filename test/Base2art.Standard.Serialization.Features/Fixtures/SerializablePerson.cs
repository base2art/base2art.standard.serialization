﻿namespace Base2art.Serialization.Fixtures
{
    using System;

    [Serializable]
    public class SerializablePerson : IPerson
    {
        public string Name { get; set; }

        public string[] Aliases { get; set; }

        public DateTime Birthday { get; set; }
    }
}