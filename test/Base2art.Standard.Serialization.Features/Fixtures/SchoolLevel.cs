﻿namespace Base2art.Serialization.Fixtures
{
    public enum SchoolLevel
    {
        MiddleSchool,
        HighSchool,
        College,
        PHD
    }
}