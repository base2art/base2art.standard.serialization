﻿namespace Base2art.Serialization.Fixtures
{
    using System;

    public interface IPerson
    {
        string Name { get; }

        DateTime Birthday { get; }

        string[] Aliases { get; }
    }
}