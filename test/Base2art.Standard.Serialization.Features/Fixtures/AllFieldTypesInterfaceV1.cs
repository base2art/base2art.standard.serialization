﻿namespace Base2art.Serialization.Fixtures
{
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using System.Xml.Linq;

    public class AllFieldTypesInterfaceV1 : AllFieldTypesNonNullableInterfaceV1
    {
        public byte[] binary_value { get; set; }

        public XmlDocument xml1_value { get; set; }

        public XElement xml2_value { get; set; }

//        public object simpleObject {get;set;}
        public Dictionary<string, object> json_value { get; set; }
        public Version version { get; set; }

        public bool? Boolean_nullable_value { get; set; }
        public decimal? Decimal_nullable_value { get; set; }
        public double? Double_nullable_value { get; set; }
        public float? Float_nullable_value { get; set; }
        public int? int_nullable_value { get; set; }
        public long? long_nullable_value { get; set; }
        public short? short_nullable_value { get; set; }
        public DateTime? date_nullable_value { get; set; }
        public DateTimeOffset? dateTimeOffset_nullable_value { get; set; }
        public DateTime? datetime_nullable_value { get; set; }
        public TimeSpan? interval_nullable_value { get; set; }
        public Guid? guid_nullable_value { get; set; }
    }
}