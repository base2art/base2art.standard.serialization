namespace Base2art.Serialization.Fixtures
{
    public class SerializableDocument
    {
        public string Name { get; set; }
        public byte[] Content { get; set; }
    }
}