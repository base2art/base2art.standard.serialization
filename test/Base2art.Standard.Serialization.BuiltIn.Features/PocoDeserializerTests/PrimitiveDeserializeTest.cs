﻿#if NUNIT
using TestClass = NUnit.Framework.TestFixtureAttribute;
using TestMethod = NUnit.Framework.TestAttribute;
using TestCleanup = NUnit.Framework.TearDownAttribute;
using TestInitialize = NUnit.Framework.SetUpAttribute;
using ClassCleanup = NUnit.Framework.TestFixtureTearDownAttribute;
using ClassInitialize = NUnit.Framework.TestFixtureSetUpAttribute;
using NUnit.Framework;
#else
#endif

namespace SimpleJsonTests.PocoDeserializerTests
{
    using Base2art.Serialization.Internals;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class PrimitiveDeserializeTest
    {
        [TestMethod]
        public void SimpleIntTests()
        {
            var json = "{\"Age\":\"12\",\"Name\": \"Simple Json\"}";

            var result = SimpleJson.DeserializeObject<X>(json, new PocoJsonSerializerStrategy());

            Assert.AreEqual(12, result.Age);
        }

        [TestMethod]
        public void SimpleDoubleTests()
        {
            var json = "{\"Salary\":\"120.50\",\"Name\": \"Simple Json\"}";

            var result = SimpleJson.DeserializeObject<X>(json, new PocoJsonSerializerStrategy());

            Assert.AreEqual(120.50, result.Salary);
        }

        private class X
        {
            public string Name { get; set; }
            public int Age { get; set; }
            public double Salary { get; set; }
        }
    }
}