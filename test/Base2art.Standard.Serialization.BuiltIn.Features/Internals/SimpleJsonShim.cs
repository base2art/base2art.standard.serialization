﻿// ReSharper disable once CheckNamespace

namespace Base2art.Serialization.Internals
{
    using System;
    using System.ComponentModel;

    public class SimpleJsonShim
    {
        // DO NOT USE
        internal static IJsonSerializerStrategy PocoJsonSerializerStrategy => new PocoJsonSerializerStrategy();

        private static DataContractJsonSerializerStrategy _dataContractJsonSerializerStrategy;

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        internal static DataContractJsonSerializerStrategy DataContractJsonSerializerStrategy =>
            _dataContractJsonSerializerStrategy ?? (_dataContractJsonSerializerStrategy = new DataContractJsonSerializerStrategy());

        public static string SerializeObject(object obj)
        {
            return SimpleJson.SerializeObject(obj, new PocoJsonSerializerStrategy());
        }
    }
}